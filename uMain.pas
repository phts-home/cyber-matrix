unit uMain;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Spin, ComCtrls, Grids, Menus, ExtCtrls, Math, ShellAPI,
  IniFiles, Buttons, Printers;

type TMainArray = Array [1..10,1..10] of Real;

type
  TFormMain = class(TForm)
    PageControl1: TPageControl;
    TabSheet2: TTabSheet;
    TabSheet3: TTabSheet;
    TabSheet4: TTabSheet;
    TabSheet5: TTabSheet;
    PageControl2: TPageControl;
    TabSheet6: TTabSheet;
    StringGrid1: TStringGrid;
    Spin_m: TSpinEdit;
    Spin_n: TSpinEdit;
    Label4: TLabel;
    Label3: TLabel;
    Label2: TLabel;
    Edit1: TEdit;
    TabSheet7: TTabSheet;
    TabSheet8: TTabSheet;
    Label1: TLabel;
    Spin_mm: TSpinEdit;
    Spin_nn: TSpinEdit;
    Label6: TLabel;
    StringGrid2: TStringGrid;
    Label7: TLabel;
    StringGrid3: TStringGrid;
    Edit2: TEdit;
    Edit3: TEdit;
    MainMenu1: TMainMenu;
    N1: TMenuItem;
    N2: TMenuItem;
    A1: TMenuItem;
    B1: TMenuItem;
    C1: TMenuItem;
    N3: TMenuItem;
    B2: TMenuItem;
    N4: TMenuItem;
    N5: TMenuItem;
    N6: TMenuItem;
    N7: TMenuItem;
    N8: TMenuItem;
    AB1: TMenuItem;
    BA1: TMenuItem;
    BA2: TMenuItem;
    BC1: TMenuItem;
    N9: TMenuItem;
    detA1: TMenuItem;
    detB1: TMenuItem;
    N11: TMenuItem;
    CAB1: TMenuItem;
    N12: TMenuItem;
    CAB2: TMenuItem;
    CBA1: TMenuItem;
    N13: TMenuItem;
    CA11: TMenuItem;
    CB11: TMenuItem;
    OpenDialog1: TOpenDialog;
    SaveDialog1: TSaveDialog;
    N14: TMenuItem;
    N10: TMenuItem;
    A01: TMenuItem;
    B01: TMenuItem;
    B3: TMenuItem;
    AE1: TMenuItem;
    BE1: TMenuItem;
    TabSheet1: TTabSheet;
    Aij1: TMenuItem;
    Bij1: TMenuItem;
    N15: TMenuItem;
    SpinEdit4: TSpinEdit;
    SpinEdit3: TSpinEdit;
    SpinEdit2: TSpinEdit;
    SpinEdit1: TSpinEdit;
    Button9: TButton;
    Button8: TButton;
    Edit7: TEdit;
    Edit6: TEdit;
    Label10: TLabel;
    Label9: TLabel;
    Label8: TLabel;
    Label5: TLabel;
    Image9: TImage;
    Image8: TImage;
    Image6: TImage;
    Image7: TImage;
    Button6: TButton;
    Button7: TButton;
    Image5: TImage;
    Button2: TButton;
    Image3: TImage;
    Image4: TImage;
    Button3: TButton;
    Button4: TButton;
    Image1: TImage;
    Image2: TImage;
    Edit5: TEdit;
    Edit4: TEdit;
    Button1: TButton;
    Button5: TButton;
    Button10: TButton;
    Button11: TButton;
    Image10: TImage;
    Image11: TImage;
    N16: TMenuItem;
    AB2: TMenuItem;
    N17: TMenuItem;
    BA3: TMenuItem;
    N18: TMenuItem;
    CA1B1: TMenuItem;
    TabSheet10: TTabSheet;
    SpinEdit5: TSpinEdit;
    Label11: TLabel;
    SpinEdit6: TSpinEdit;
    Label12: TLabel;
    Label13: TLabel;
    SpinEdit7: TSpinEdit;
    SpinEdit8: TSpinEdit;
    Label14: TLabel;
    N19: TMenuItem;
    N21: TMenuItem;
    N22: TMenuItem;
    N23: TMenuItem;
    N24: TMenuItem;
    AAT1: TMenuItem;
    BBT1: TMenuItem;
    N25: TMenuItem;
    CAT1: TMenuItem;
    CBT1: TMenuItem;
    N26: TMenuItem;
    N27: TMenuItem;
    SaveDialog2: TSaveDialog;
    Button12: TButton;
    Button13: TButton;
    N28: TMenuItem;
    N29: TMenuItem;
    N30: TMenuItem;
    N31: TMenuItem;
    Button14: TButton;
    N32: TMenuItem;
    Label15: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    N33: TMenuItem;
    N34: TMenuItem;
    N35: TMenuItem;
    N36: TMenuItem;
    TabSheet9: TTabSheet;
    Image12: TImage;
    Image13: TImage;
    Button16: TButton;
    Button17: TButton;
    N37: TMenuItem;
    N38: TMenuItem;
    N39: TMenuItem;
    N40: TMenuItem;
    N41: TMenuItem;
    SpeedButton1: TSpeedButton;
    SpeedButton2: TSpeedButton;
    SpeedButton3: TSpeedButton;
    SpeedButton4: TSpeedButton;
    SpeedButton5: TSpeedButton;
    N42: TMenuItem;
    N43: TMenuItem;
    N20: TMenuItem;
    PrintDialog1: TPrintDialog;
    PrinterSetupDialog1: TPrinterSetupDialog;
    ReadMetxt1: TMenuItem;
    N44: TMenuItem;
    procedure Spin_mChange(Sender: TObject);
    procedure StringGrid1SelectCell(Sender: TObject; ACol, ARow: Integer;
      var CanSelect: Boolean);
    procedure Edit1KeyPress(Sender: TObject; var Key: Char);
    procedure Edit1Change(Sender: TObject);
    procedure Spin_nChange(Sender: TObject);
    procedure Label4Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure Edit2Change(Sender: TObject);
    procedure StringGrid2SelectCell(Sender: TObject; ACol, ARow: Integer;
      var CanSelect: Boolean);
    procedure Spin_mmChange(Sender: TObject);
    procedure Spin_nnChange(Sender: TObject);
    procedure Label7Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Edit2KeyPress(Sender: TObject; var Key: Char);
    procedure StringGrid3SelectCell(Sender: TObject; ACol, ARow: Integer;
      var CanSelect: Boolean);
    procedure TabSheet6Show(Sender: TObject);
    procedure TabSheet7Show(Sender: TObject);
    procedure TabSheet8Show(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button5Click(Sender: TObject);
    procedure A1Click(Sender: TObject);
    procedure B1Click(Sender: TObject);
    procedure C1Click(Sender: TObject);
    procedure N3Click(Sender: TObject);
    procedure B2Click(Sender: TObject);
    procedure detA1Click(Sender: TObject);
    procedure detB1Click(Sender: TObject);
    procedure CAB2Click(Sender: TObject);
    procedure CBA1Click(Sender: TObject);
    procedure CAB1Click(Sender: TObject);
    procedure CA11Click(Sender: TObject);
    procedure CB11Click(Sender: TObject);
    procedure Button6Click(Sender: TObject);
    procedure Button7Click(Sender: TObject);
    procedure AE1Click(Sender: TObject);
    procedure A01Click(Sender: TObject);
    procedure AB1Click(Sender: TObject);
    procedure BA1Click(Sender: TObject);
    procedure BE1Click(Sender: TObject);
    procedure B01Click(Sender: TObject);
    procedure BA2Click(Sender: TObject);
    procedure BC1Click(Sender: TObject);
    procedure N4Click(Sender: TObject);
    procedure N6Click(Sender: TObject);
    procedure Button8Click(Sender: TObject);
    procedure Button9Click(Sender: TObject);
    procedure Aij1Click(Sender: TObject);
    procedure Bij1Click(Sender: TObject);
    procedure Button10Click(Sender: TObject);
    procedure Button11Click(Sender: TObject);
    procedure AB2Click(Sender: TObject);
    procedure BA3Click(Sender: TObject);
    procedure Button12Click(Sender: TObject);
    procedure CA1B1Click(Sender: TObject);
    procedure Edit1Click(Sender: TObject);
    procedure Edit2Click(Sender: TObject);
    procedure Edit3Click(Sender: TObject);
    procedure Edit1KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure Edit2KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure Edit3KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SpinEdit5Change(Sender: TObject);
    procedure SpinEdit6Change(Sender: TObject);
    procedure SpinEdit7Change(Sender: TObject);
    procedure SpinEdit8Change(Sender: TObject);
    procedure N21Click(Sender: TObject);
    procedure N22Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure AAT1Click(Sender: TObject);
    procedure CAT1Click(Sender: TObject);
    procedure CBT1Click(Sender: TObject);
    procedure BBT1Click(Sender: TObject);
    procedure N27Click(Sender: TObject);
    procedure Button13Click(Sender: TObject);
    procedure N29Click(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure N30Click(Sender: TObject);
    procedure Button14Click(Sender: TObject);
    procedure N32Click(Sender: TObject);
    procedure N33Click(Sender: TObject);
    procedure N34Click(Sender: TObject);
    procedure Button16Click(Sender: TObject);
    procedure Button17Click(Sender: TObject);
    procedure N37Click(Sender: TObject);
    procedure N38Click(Sender: TObject);
    procedure N40Click(Sender: TObject);
    procedure N42Click(Sender: TObject);
    procedure N20Click(Sender: TObject);
    procedure ReadMetxt1Click(Sender: TObject);
  private
    { Private declarations }
  public
    mA, mB: Array [1..5,1..5] of Real;
    cI1,cJ1,cI2,cJ2,cI3,cJ3: Integer;
    TempArray: TMainArray;
    ResultFile: TextFile;
    cfg:TIniFile;
//    DetRes: Array [0..10] of Real;
    procedure FromGridToArray (Source: TStringGrid; var New: TMainArray);
    procedure FromArrayToGrid (Source: TMainArray; Heigth, Wight: Byte; New: TStringGrid);

    function Calculate (Size: Byte; Ar: TMainArray;  OnRow, OnCol: Byte;
      SomeVal: Byte): Real;
    function StartComplement(A: TStringGrid; I, J: Byte; Symbol: Char): Real;
    function Complement (A: TStringGrid; I, J: Byte; Symbol: Char): Real;
    function StartDetA (A: TStringGrid; I, J: Byte; Symbol: Char): Real;
    function DetA (A: TStringGrid; I, J: Byte; Symbol: Char): Real;
    procedure StartSumm;
    procedure Summ;
    procedure StartMultiple (A, B: TStringGrid; SymbolA, SymbolB: Char);
    procedure Multiple (A, B: TStringGrid; SymbolA, SymbolB: Char);
    procedure StartTranspon (A: TStringGrid; Symbol: Char);
    procedure Transpon (A: TStringGrid; Symbol: Char);
    procedure StartTranspS (A: TStringGrid; Symbol: Char);
    procedure TranspS (A: TStringGrid; Symbol: Char);
    procedure StartReverse (A: TStringGrid; det: Real; Symbol: Char);
    procedure Reverse (A: TStringGrid; Symbol: Char);
    procedure Equation;
    procedure EquationCramer;
    procedure EquationGaus;

    procedure ZeroMatrix (Source: TStringGrid);
    procedure UnitMatrix (Source: TStringGrid);
    procedure CompareMatrix (Source, New: TStringGrid);
    procedure Switch (Source: TStringGrid);

    procedure SelectCellHorizontal (SGrid: TStringGrid; I, J, Direction: Integer);
    procedure SelectCellVertical (SGrid: TStringGrid; I, J, Direction: Integer);
    procedure OpenMatrix (Matrix: TStringGrid; Spin1, Spin2: TSpinEdit; Path: String);
    procedure SaveMatrix (Matrix: TStringGrid; Path: String);
    procedure SaveLog (Path: String);
    procedure PrintStrings (Strings: TStrings);
    { Public declarations }
  end;



var
  FormMain: TFormMain;

implementation

uses uAbout, uAdd, uRandom, uResult;

{$R *.dfm}

procedure TFormMain.FromArrayToGrid(Source: TMainArray; Heigth, Wight: Byte; New: TStringGrid);
var i, j: Byte;
begin
New.ColCount:=Wight;
New.RowCount:=Heigth;
for i:=0 to 9 do
  for j:=0 to 9 do
    begin
    New.Cells[i,j]:=FloatToStr(Source[i+1,j+1]);
    end;
end;

procedure TFormMain.FromGridToArray(Source: TStringGrid; var New: TMainArray);
var i, j: Byte;
begin
for i:=0 to 9 do
  for j:=0 to 9 do
    begin
    New[i+1,j+1]:=StrToFloat(Source.Cells[i,j]);
    end;
end;

function TFormMain.Calculate (Size: Byte; Ar: TMainArray;  OnRow, OnCol: Byte;
  SomeVal: Byte): Real;
var i, ii, jj, iii, jjj: Byte;
    one: Integer;
    Arr: TMainArray;
    CurrentCell, SecRes: Real;
begin
Result:=0;
//if {(Size=SomeVal-1)or}(Size<SomeVal-1) then Write(ResultFile,'[');

if Size<>1 then
  begin
  Arr:=Ar;

  for i:=1 to Size do
    begin
    if (i+OnRow+OnCol)mod 2=0 then one:=1 else one:=-1;

    if OnRow<>0 then
      begin
      CurrentCell:=Ar[i,OnRow];
      end else
      begin
      CurrentCell:=Ar[OnCol,i];
      end;

    iii:=1;
    for ii:=1 to Size do
      begin
      jjj:=1;
//      if (ii<>OnRow)and(ii<>OnCol) then
      if ((ii<>OnRow)and(OnRow<>0))or((ii<>i)and(OnCol<>0)) then
        begin
        for jj:=1 to Size do
          begin
//          if (jj<>i)and(jj<>OnCol) then
          if ((jj<>i)and(OnRow<>0))or((jj<>OnCol)and(OnCol<>0)) then
            begin
            Arr[jjj,iii]:=Ar[jj,ii];
            inc(jjj);
            end;
          end;
        inc(iii);
        end;
      end;

//    if (i=1)and(Size<>SomeVal) then WriteLn(ResultFile,'{');
{    for x:=1 to Size do
      begin
      for y:=1 to Size do
        begin
        Write(ResultFile,FloatToStr(Arr[y,x]),'|');
        end;
      WriteLn(ResultFile);
      end;      
    WriteLn(ResultFile);
    WriteLn(ResultFile,' =');
    WriteLn(ResultFile);  }
//    if (i=1)and(Size<>SomeVal) then WriteLn(ResultFile);
//    Write(ResultFile,'{ ');
//    if one=-1 then Write(ResultFile,'(',one,') * ');
    if CurrentCell>=0
      then Write(ResultFile,FloatToStr(CurrentCell),'*')
      else Write(ResultFile,'(',FloatToStr(CurrentCell),')*');
    if Size>2 then Write(ResultFile,'(');
//    if {(i=1)and}(Size<=2)then Write(ResultFile,'(');
    SecRes:=Calculate(Size-1,Arr,1,0,SomeVal);
    Result:=Result+one*CurrentCell*SecRes;

//    Write(ResultFile,'(',one,') * ',FloatToStr(CurrentCell),' * {');
    if Size=2 then
      if SecRes>=0
        then Write(ResultFile,FloatToStr(SecRes))
        else Write(ResultFile,'(',FloatToStr(SecRes),')');
    if Size>2 then Write(ResultFile,')');
//    if Size=SomeVal then DetRes[i]:=Result;
//    if Size=SomeVal then
//      begin
//    if (i=1)and(Size<>SomeVal) then WriteLn(ResultFile);
//    Write(ResultFile,' * ',FloatToStr(CurrentCell),') * (',one,')');
//    Write(ResultFile,' * ',FloatToStr(CurrentCell));

//    Write(ResultFile,' * ',FloatToStr(CurrentCell));
//    {if (Size<>SomeVal) then} Write(ResultFile,'}');
//    Write(ResultFile,' * (',one,')');
//{    if (i=Size)and(Size<>SomeVal) then }Write(ResultFile,']');
//    Write(ResultFile,' * (',one,')');
//    if (i<>Size)and(Size<>SomeVal) then Write(ResultFile,' + ');
//      end;

//    Write(ResultFile,'}');

//    Write(ResultFile,' }');
    if (i<>Size)and(Size<>SomeVal) then
      begin
      if one=-1
        then Write(ResultFile,' + ')
        else Write(ResultFile,' - ');
      end;
    if (Size=SomeVal)and(i<>Size) then
      begin
      WriteLn(ResultFile);
//      WriteLn(ResultFile);
//      WriteLn(ResultFile,'+');
      if one=1
        then WriteLn(ResultFile,'-')
        else WriteLn(ResultFile,'+');
//      WriteLn(ResultFile);
      end;
{    if Size=SomeVal then
      begin
      WriteLn(ResultFile,'>>> (',IntToStr(one)+')*',FloatToStr(CurrentCell),'*',FloatToStr(SecRes));
      if i<>Size then begin WriteLn(ResultFile,'    +'); WriteLn(ResultFile); end;
      end else
      begin
      Write(ResultFile,'(',IntToStr(one)+')*',FloatToStr(CurrentCell),'*',FloatToStr(SecRes),' ');
      if i<>Size then begin WriteLn(ResultFile,'+'); WriteLn(ResultFile); end; }
//      if (i=Size)and(Size<>SomeVal) then begin WriteLn(ResultFile,' = ',FloatToStr(Result),'}'); WriteLn(ResultFile); end;
//      end;


//    WriteLn(ResultFile,'('+IntToStr(one)+')*'+FloatToStr(CurrentCell)+'*'+FloatToStr(SecRes));
//    if not((i=Size)and(Size=SomeVal)) then WriteLn(ResultFile,'+');
//    if i<>Size then WriteLn(ResultFile,'+')
//        else WriteLn(ResultFile,'}');
    end;
  end else
  begin
//  if Size<>SomeVal-1 then Write(ResultFile,'(');
//  if SomeVal>2
//    then Write(ResultFile,'{',FloatToStr(Ar[1,1]){,' * '})
    {else }
//  Write(ResultFile,'<');
//  Write(ResultFile,FloatToStr(Ar[1,1]));
  Result:=Ar[1,1];
  end;
end;

function TFormMain.StartComplement(A: TStringGrid; I, J: Byte; Symbol: Char): Real;//���. ����������
var x, y, iii, jjj: Byte;
    one: Integer;
    AA: TMainArray;
begin
Write(ResultFile,Symbol+'('+IntToStr(I)+','+IntToStr(J)+') = ');
//WriteLn(ResultFile);

if (I+J)mod 2=0 then one:=1 else one:=-1;
iii:=1;
for x:=0 to A.RowCount-1 do
  begin
  jjj:=1;
  if x<>J-1 then
    begin
    for y:=0 to A.RowCount-1 do
      begin
      if y<>I-1 then
        begin
        AA[iii,jjj]:=StrToFloat(A.Cells[x,y]);
        inc(jjj);
        end;
      end;
    inc(iii);
    end;
  end;
if one=-1 then WriteLn(ResultFile,'(-1)*(')
  else WriteLn(ResultFile);
Result:=one*Calculate(A.RowCount-1,AA,1,0,A.RowCount-1);


if one=-1 then WriteLn(ResultFile,')')
  else WriteLn(ResultFile);
WriteLn(ResultFile,'= '+FloatToStr(Result));
WriteLn(ResultFile);
end;

function TFormMain.Complement(A: TStringGrid; I, J: Byte; Symbol: Char): Real;//���. ����������
var x, y: Byte;
begin
try
  Append(ResultFile);
  except Rewrite(ResultFile);
  end;

WriteLn(ResultFile,'-------- �������������� ���������� �������� '+LowerCase(Symbol)+'('
    +IntToStr(I)+','+IntToStr(J)+') ������� '+Symbol+' ------');
WriteLn(ResultFile,'-------- '+DateToStr(Date)+' '+TimeToStr(Time));
WriteLn(ResultFile);

WriteLn(ResultFile,'['+Symbol+'] =');
WriteLn(ResultFile);

for x:=0 to A.RowCount-1 do
  begin
  for y:=0 to A.ColCount-1 do
    begin
    Write(ResultFile, A.Cells[y,x]+'|');
    end;
  WriteLn(ResultFile);
  end;
WriteLn(ResultFile);

Result:=StartComplement(A,I,J,Symbol);

CloseFile(ResultFile);
end;

function TFormMain.StartDetA(A: TStringGrid; I, J: Byte; Symbol: Char): Real; //������������
var x, y: Word;
    AA: TMainArray;
begin
WriteLn(ResultFile,'Det('+Symbol+') =');
WriteLn(ResultFile);
for x:=0 to A.RowCount-1 do
  for y:=0 to A.ColCount-1 do
    AA[x+1,y+1]:=StrToFloat(A.Cells[x,y]);
Result:=Calculate(A.RowCount,AA,I,J,A.RowCount);
WriteLn(ResultFile);
WriteLn(ResultFile);

WriteLn(ResultFile,'= '+FloatToStr(Result));
WriteLn(ResultFile);
end;

function TFormMain.DetA (A: TStringGrid; I, J: Byte; Symbol: Char): Real;//������������
var x, y: Word;
begin
try
  Append(ResultFile);
  except Rewrite(ResultFile);
  end;
if I<>0 then
  begin
  WriteLn(ResultFile,'-------- ������������ ������� '+Symbol+' �� i-�� ������ (i=',I,') ------');
  end else
  begin
  WriteLn(ResultFile,'-------- ������������ ������� '+Symbol+' �� j-�� ������� (j=',J,') ------');
  end;

WriteLn(ResultFile,'-------- '+DateToStr(Date)+' '+TimeToStr(Time));
WriteLn(ResultFile);

{if I<>0 then
  begin
  WriteLn(ResultFile,'i='+IntToStr(I));
  end else
  begin
  WriteLn(ResultFile,'j='+IntToStr(J));
  end;
WriteLn(ResultFile);  }

WriteLn(ResultFile,'['+Symbol+'] =');
WriteLn(ResultFile);

for x:=0 to A.RowCount-1 do
  begin
  for y:=0 to A.ColCount-1 do
    begin
    Write(ResultFile, A.Cells[y,x]+'|');
    end;
  WriteLn(ResultFile);
  end;
WriteLn(ResultFile);

Result:=StartDetA(A, I, J, Symbol);

CloseFile(ResultFile);
end;

procedure TFormMain.StartMultiple(A, B: TStringGrid; SymbolA, SymbolB: Char);//���������
var i, j, m: Integer;
    Line: Real;
begin
WriteLn(ResultFile,'['+SymbolA+'] =');
WriteLn(ResultFile);

for i:=0 to A.RowCount-1 do
  begin
  for j:=0 to A.ColCount-1 do
    begin
    Write(ResultFile, A.Cells[j,i],'|');
    end;
  WriteLn(ResultFile);
  end;
WriteLn(ResultFile);

WriteLn(ResultFile,'['+SymbolB+'] =');
WriteLn(ResultFile);

for i:=0 to B.RowCount-1 do
  begin
  for j:=0 to B.ColCount-1 do
    begin
    Write(ResultFile, B.Cells[j,i],'|');
    end;
  WriteLn(ResultFile);
  end;
WriteLn(ResultFile);

WriteLn(ResultFile,'[C] = ['+SymbolA+']*['+SymbolB+'] =');
WriteLn(ResultFile);

StringGrid3.RowCount:=A.RowCount;
StringGrid3.ColCount:=B.ColCount;
for i:=0 to A.RowCount-1 do
  begin
  for j:=0 to B.ColCount-1 do
    begin
    Line:=0;
    for m:=0 to A.ColCount-1 do
      begin
      Line:=Line+StrToFloat(A.Cells[m,i])*StrToFloat(B.Cells[j,m]);
      if StrToFloat(A.Cells[m,i])<0 then Write(ResultFile,'(');
      Write(ResultFile, FloatToStr(StrToFloat(A.Cells[m,i])){:2:2});
      if StrToFloat(A.Cells[m,i])<0 then Write(ResultFile,')');
      Write(ResultFile, '*');
      if StrToFloat(B.Cells[j,m])<0 then Write(ResultFile,'(');
      Write(ResultFile, FloatToStr(StrToFloat(B.Cells[j,m])){:2:2});
      if StrToFloat(B.Cells[j,m])<0 then Write(ResultFile,')');
      if m<>A.ColCount-1 then Write(ResultFile, ' + ');
      end;
    StringGrid3.Cells[j,i]:=FloatToStr(Line);
    Write(ResultFile,' | ');
    end;
  WriteLn(ResultFile);
  end;
WriteLn(ResultFile);

WriteLn(ResultFile,'[C] =');
WriteLn(ResultFile);
for i:=0 to StringGrid3.RowCount-1 do
  begin
  for j:=0 to StringGrid3.ColCount-1 do
    begin
    Write(ResultFile, StrToFloat(StringGrid3.Cells[j,i]):2:2,'|');
    end;
  WriteLn(ResultFile);
  end;
WriteLn(ResultFile);
end;

procedure TFormMain.Multiple (A, B: TStringGrid; SymbolA, SymbolB: Char);//���������
begin
try
  Append(ResultFile);
  except Rewrite(ResultFile);
  end;

WriteLn(ResultFile,'-------- ������������ ������ '+SymbolA+' � '+SymbolB+' ------');
WriteLn(ResultFile,'-------- '+DateToStr(Date)+' '+TimeToStr(Time));
WriteLn(ResultFile);

StartMultiple(A, B, SymbolA, SymbolB);

PageControl2.Pages[2].Show;
SelectCellHorizontal(StringGrid3,-1,0,1);
CloseFile(ResultFile);
end;

procedure TFormMain.StartSumm;                                //�����
var i, j: Byte;
begin
WriteLn(ResultFile,'[A] =');
WriteLn(ResultFile);

for i:=0 to StringGrid1.RowCount-1 do
  begin
  for j:=0 to StringGrid1.ColCount-1 do
    begin
    Write(ResultFile, StringGrid1.Cells[j,i]+'|');
    end;
  WriteLn(ResultFile);
  end;
WriteLn(ResultFile);

WriteLn(ResultFile,'[B] =');
WriteLn(ResultFile);

for i:=0 to StringGrid2.RowCount-1 do
  begin
  for j:=0 to StringGrid2.ColCount-1 do
    begin
    Write(ResultFile, StringGrid2.Cells[j,i]+'|');
    end;
  WriteLn(ResultFile);
  end;
WriteLn(ResultFile);

WriteLn(ResultFile,'[C] = [A]+[B] =');
WriteLn(ResultFile);

StringGrid3.RowCount:=StringGrid1.RowCount;
StringGrid3.ColCount:=StringGrid1.ColCount;
for i:=0 to StringGrid1.RowCount-1 do
  begin
  for j:=0 to StringGrid1.ColCount-1 do
    begin
    StringGrid3.Cells[j,i]:=FloatToStr(StrToFloat(StringGrid1.Cells[j,i])
        +StrToFloat(StringGrid2.Cells[j,i]));
    Write(ResultFile,StringGrid1.Cells[j,i]);
    if StrToFloat(StringGrid2.Cells[j,i])>=0
      then Write(ResultFile,'+')
      else Write(ResultFile,'-');
    Write(ResultFile,FloatToStr(Abs(StrToFloat(StringGrid2.Cells[j,i])))+' | ');
    end;
  WriteLn(ResultFile);
  end;
WriteLn(ResultFile);

WriteLn(ResultFile,'[C] =');
WriteLn(ResultFile);
for i:=0 to StringGrid3.RowCount-1 do
  begin
  for j:=0 to StringGrid3.ColCount-1 do
    begin
    Write(ResultFile, StringGrid3.Cells[j,i]+'|');
    end;
  WriteLn(ResultFile);
  end;
end;

procedure TFormMain.Summ;                                      //�����
begin
try
  Append(ResultFile);
  except Rewrite(ResultFile);
  end;

WriteLn(ResultFile,'-------- ����� ������ A � B ------');
WriteLn(ResultFile,'-------- '+DateToStr(Date)+' '+TimeToStr(Time));
WriteLn(ResultFile);

StartSumm;

PageControl2.Pages[2].Show;
SelectCellHorizontal(StringGrid3,-1,0,1);
CloseFile(ResultFile);
end;

procedure TFormMain.StartTranspon(A: TStringGrid; Symbol: Char);//����������������
var x, y: Byte;
begin
StringGrid3.ColCount:=A.RowCount;
StringGrid3.RowCount:=A.ColCount;
for x:=0 to A.RowCount-1 do
  for y:=0 to A.ColCount-1 do
    begin
    StringGrid3.Cells[x,y]:=A.Cells[y,x];
    end;

WriteLn(ResultFile,'('+Symbol+')T =');
WriteLn(ResultFile);
for x:=0 to StringGrid3.RowCount-1 do
  begin
  for y:=0 to StringGrid3.ColCount-1 do
    begin
    Write(ResultFile, StringGrid3.Cells[y,x]+'|');
    end;
  WriteLn(ResultFile);
  end;
WriteLn(ResultFile);
end;

procedure TFormMain.Transpon(A: TStringGrid; Symbol: Char); //����������������
var x, y: Byte;
begin
try
  Append(ResultFile);
  except Rewrite(ResultFile);
  end;

WriteLn(ResultFile,'-------- ���������������� ������� '+Symbol+' ------');
WriteLn(ResultFile,'-------- '+DateToStr(Date)+' '+TimeToStr(Time));
WriteLn(ResultFile);

WriteLn(ResultFile,'['+Symbol+'] =');
WriteLn(ResultFile);

for x:=0 to A.RowCount-1 do
  begin
  for y:=0 to A.ColCount-1 do
    begin
    Write(ResultFile, A.Cells[y,x]+'|');
    end;
  WriteLn(ResultFile);
  end;
WriteLn(ResultFile);

StartTranspon(A,Symbol);

PageControl2.Pages[2].Show;
SelectCellHorizontal(StringGrid3,-1,0,1);
CloseFile(ResultFile);
end;

procedure TFormMain.StartTranspS(A: TStringGrid; Symbol: Char);      // (A*)T
var x, y: Byte;
begin
StringGrid3.ColCount:=A.ColCount;
StringGrid3.RowCount:=A.ColCount;
for x:=0 to A.ColCount-1 do
  for y:=0 to A.ColCount-1 do
    begin
    StringGrid3.Cells[x,y]:=FloatToStr(StartComplement(A, x+1, y+1, Symbol));
    end;

WriteLn(ResultFile,'('+Symbol+'*)T =');
WriteLn(ResultFile);
for x:=0 to StringGrid3.RowCount-1 do
  begin
  for y:=0 to StringGrid3.ColCount-1 do
    begin
    Write(ResultFile, StringGrid3.Cells[y,x]+'|');
    end;
  WriteLn(ResultFile);
  end;
WriteLn(ResultFile);
end;

procedure TFormMain.TranspS(A: TStringGrid; Symbol: Char);       // (A*)T
var x, y: Byte;
begin
try
  Append(ResultFile);
  except Rewrite(ResultFile);
  end;

WriteLn(ResultFile,'-------- ('+Symbol+'*)T ------');
WriteLn(ResultFile,'-------- '+DateToStr(Date)+' '+TimeToStr(Time));
WriteLn(ResultFile);

WriteLn(ResultFile,'['+Symbol+'] =');
WriteLn(ResultFile);

for x:=0 to A.RowCount-1 do
  begin
  for y:=0 to A.ColCount-1 do
    begin
    Write(ResultFile, A.Cells[y,x]+'|');
    end;
  WriteLn(ResultFile);
  end;
WriteLn(ResultFile);

StringGrid3.ColCount:=A.ColCount;
StringGrid3.RowCount:=A.ColCount;
StartTranspS(A, Symbol);

PageControl2.Pages[2].Show;
SelectCellHorizontal(StringGrid3,-1,0,1);
WriteLn(ResultFile);
CloseFile(ResultFile);
end;

procedure TFormMain.StartReverse(A: TStringGrid; det: Real; Symbol: Char);//��������
var x, y: Byte;
begin
WriteLn(ResultFile,'det('+Symbol+') = '+FloatToStr(det));
WriteLn(ResultFile);
StartTranspS(A, Symbol);

WriteLn(ResultFile,Symbol+'^(-1) =');
WriteLn(ResultFile);
for x:=0 to A.ColCount-1 do
  begin
  for y:=0 to A.ColCount-1 do
    begin
    Write(ResultFile, StringGrid3.Cells[y,x]+'/'+FloatToStr(det)+' | ');
    StringGrid3.Cells[y,x]:=FloatToStr(StrToFloat(StringGrid3.Cells[y,x])/det);
    end;
  WriteLn(ResultFile);
  end;
WriteLn(ResultFile);

WriteLn(ResultFile,Symbol+'^(-1) =');
WriteLn(ResultFile);
for x:=0 to StringGrid3.RowCount-1 do
  begin
  for y:=0 to StringGrid3.ColCount-1 do
    begin
    Write(ResultFile, StrToFloat(StringGrid3.Cells[y,x]):2:2, '|');
    end;
  WriteLn(ResultFile);
  end;
WriteLn(ResultFile);
end;

procedure TFormMain.Reverse(A: TStringGrid; Symbol: Char);     //��������
var x, y: Byte;
    det: Real;
begin
try
  Append(ResultFile);
  except Rewrite(ResultFile);
  end;
WriteLn(ResultFile,'-------- �������� ������� ------');
WriteLn(ResultFile,'-------- '+DateToStr(Date)+' '+TimeToStr(Time));
WriteLn(ResultFile);

WriteLn(ResultFile,'['+Symbol+'] =');
WriteLn(ResultFile);

  for x:=0 to A.RowCount-1 do
    begin
    for y:=0 to A.ColCount-1 do
      begin
      Write(ResultFile, A.Cells[y,x]+'|');
      end;
    WriteLn(ResultFile);
    end;
WriteLn(ResultFile);

det:=StartDetA(A, 1, 0, Symbol);
if det<>0 then
  begin

  StartReverse(A, det, Symbol);

  PageControl2.Pages[2].Show;
  SelectCellHorizontal(StringGrid3,-1,0,1);

  end else
  begin
  WriteLn(ResultFile,'� ������ ������� �������� �� ����������');
  WriteLn(ResultFile);
  Application.MessageBox(PChar('� ������ ������� �������� �� ����������'),PChar(FormMain.Caption),MB_OK+MB_ICONINFORMATION);
  end;
CloseFile(ResultFile);
end;

procedure TFormMain.Equation;                 //������� ��-��. ��������� ������
var det: Real;
    h, w, x, y: Byte;
begin
try
  Append(ResultFile);
  except Rewrite(ResultFile);
  end;
WriteLn(ResultFile,'-------- ������� ������� �������� ���������. ��������� ������ ------');
WriteLn(ResultFile,'-------- '+DateToStr(Date)+' '+TimeToStr(Time));
WriteLn(ResultFile);

for x:=0 to StringGrid1.RowCount-1 do
  begin
  for y:=0 to StringGrid1.ColCount-1 do
    begin
    if y<>0 then
      if StrToFloat(StringGrid1.Cells[y,x])>=0
        then Write(ResultFile,' + ')
        else Write(ResultFile,' - ');
    Write(ResultFile,FloatToStr(Abs(StrToFloat(StringGrid1.Cells[y,x]))),'*x',y+1);
//    if StrToFloat(StringGrid1.Cells[y,x])<>0 then
//      begin
{    if StrToFloat(StringGrid1.Cells[y,x])>=0
      then Write(ResultFile,StringGrid1.Cells[y,x],'*x',y+1)
      else Write(ResultFile,'(',StringGrid1.Cells[y,x],')*x',y+1);   }
//      end;
//    Write(ResultFile,StringGrid1.Cells[y,x],'*x',y+1);

    end;
  WriteLn(ResultFile,' = '+StringGrid2.Cells[0,x]);
  end;
WriteLn(ResultFile);

WriteLn(ResultFile,'[A] =');
WriteLn(ResultFile);

for x:=0 to StringGrid1.RowCount-1 do
  begin
  for y:=0 to StringGrid1.ColCount-1 do
    begin
    Write(ResultFile, StringGrid1.Cells[y,x]+'|');
    end;
  WriteLn(ResultFile);
  end;
WriteLn(ResultFile);

det:=StartDetA(StringGrid1, 1, 0, 'A');
if det<>0 then
  begin
  h:=StringGrid1.RowCount;
  w:=StringGrid1.ColCount;
  FromGridToArray(StringGrid1, TempArray);
  StartReverse(StringGrid1, det, 'A');
  PageControl2.Pages[0].Show;
  WriteLn(ResultFile,'[A] = A^(-1)');
  WriteLn(ResultFile);
  CompareMatrix(StringGrid3,StringGrid1);
  StartMultiple(StringGrid1,StringGrid2,'A','B');
  FromArrayToGrid(TempArray, h, w, StringGrid1);
  WriteLn(ResultFile,'[X] = [C]');
  WriteLn(ResultFile);
  for x:=0 to StringGrid3.RowCount-1 do
    begin
    WriteLn(ResultFile,'x',x+1,' = '+StringGrid3.Cells[0,x]);
    end;
  WriteLn(ResultFile);

  PageControl2.Pages[2].Show;
  SelectCellHorizontal(StringGrid3,-1,0,1);
  end else
  begin
  WriteLn(ResultFile,'������� ����� ���������� ����� ������� ��� �� ����� �������');
  WriteLn(ResultFile);
  Application.MessageBox(PChar('������� ����� ���������� ����� ������� ��� �� ����� �������'),PChar(FormMain.Caption),MB_OK+MB_ICONINFORMATION);
  PageControl2.Pages[2].Show;
  ZeroMatrix(StringGrid3);
  PageControl2.Pages[0].Show;
  end;
CloseFile(ResultFile);
end;

procedure TFormMain.EquationCramer;                 //������� ��-�� �� �������
var Ar: TMainArray;
    x, y, n: Byte;
    DetX, det: Real;
    NonZeroDetX: Boolean;
begin
try
  Append(ResultFile);
  except Rewrite(ResultFile);
  end;
WriteLn(ResultFile,'-------- ������� ������� �������� ���������. ����� ������� ------');
WriteLn(ResultFile,'-------- '+DateToStr(Date)+' '+TimeToStr(Time));
WriteLn(ResultFile);

NonZeroDetX:=False;

for x:=0 to StringGrid1.RowCount-1 do
  begin
  for y:=0 to StringGrid1.ColCount-1 do
    begin
    if y<>0 then
      if StrToFloat(StringGrid1.Cells[y,x])>=0
        then Write(ResultFile,' + ')
        else Write(ResultFile,' - ');
    Write(ResultFile,FloatToStr(Abs(StrToFloat(StringGrid1.Cells[y,x]))),'*x',y+1);
{    Write(ResultFile,StringGrid1.Cells[y,x],'*x',y+1);
    if y<>StringGrid1.ColCount-1 then Write(ResultFile,' + ');  }
    end;
  WriteLn(ResultFile,' = '+StringGrid2.Cells[0,x]);
  end;
WriteLn(ResultFile);

WriteLn(ResultFile,'[A] =');
WriteLn(ResultFile);

for x:=0 to StringGrid1.RowCount-1 do
  begin
  for y:=0 to StringGrid1.ColCount-1 do
    begin
    Write(ResultFile, StringGrid1.Cells[y,x]+'|');
    end;
  WriteLn(ResultFile);
  end;
WriteLn(ResultFile);

StringGrid3.RowCount:=StringGrid2.RowCount;
StringGrid3.ColCount:=StringGrid2.ColCount;

det:=StartDetA(StringGrid1, 1, 0, 'A');

for n:=0 to StringGrid1.ColCount-1 do
  begin
  FromGridToArray(StringGrid1,Ar);

  WriteLn(ResultFile,'Delta x',n+1,' =');
  WriteLn(ResultFile);
  for x:=0 to StringGrid1.ColCount-1 do
    begin
    if x=n then
      begin
      for y:=0 to StringGrid1.RowCount-1 do
        begin
        Ar[y+1,x+1]:=StrToFloat(StringGrid2.Cells[0,y]);
        end;
      end else
      begin
      for y:=0 to StringGrid1.RowCount-1 do
        begin
        Ar[y+1,x+1]:=StrToFloat(StringGrid1.Cells[x,y]);
        end;
      end;
    end;

  for x:=0 to StringGrid1.ColCount-1 do
    begin
    for y:=0 to StringGrid1.RowCount-1 do
      begin
      Write(ResultFile,FloatToStr(Ar[x+1,y+1]),'|');
      end;
    WriteLn(ResultFile);
    end;
  WriteLn(ResultFile);

  WriteLn(ResultFile,'Delta x',n+1,' =');
  WriteLn(ResultFile);
  DetX:=Calculate(StringGrid1.ColCount,Ar,1,0,StringGrid1.ColCount);
  WriteLn(ResultFile);
  WriteLn(ResultFile);
  WriteLn(ResultFile,'= '+FloatToStr(DetX));
  WriteLn(ResultFile);
  if DetX<>0 then NonZeroDetX:=True;
  WriteLn(ResultFile,'x',n+1,' = Delta x',n+1,' / Det(A) = ',FloatToStr(DetX)+' / '+FloatToStr(det));
  WriteLn(ResultFile);
  if det<>0 then
    begin
    StringGrid3.Cells[0,n]:=FloatToStr(DetX/det);
    end;
  end;

if det=0then
  begin
  if NonZeroDetX then
    begin
    WriteLn(ResultFile,'������� �� ����� �������');
    WriteLn(ResultFile);
    Application.MessageBox(PChar('������� �� ����� �������'),PChar(FormMain.Caption),MB_OK+MB_ICONINFORMATION);
    end else
    begin
    WriteLn(ResultFile,'������� ����� ���������� ����� �������');
    WriteLn(ResultFile);
    Application.MessageBox(PChar('������� ����� ���������� ����� �������'),PChar(FormMain.Caption),MB_OK+MB_ICONINFORMATION);
    end;
  PageControl2.Pages[0].Show;
  SelectCellHorizontal(StringGrid1,-1,0,1);
  end else
  begin
  for x:=0 to StringGrid3.RowCount-1 do
    begin
    WriteLn(ResultFile,'x',x+1,' = '+StringGrid3.Cells[0,x]);
    end;
  WriteLn(ResultFile);
  PageControl2.Pages[2].Show;
  SelectCellHorizontal(StringGrid3,-1,0,1);
  end;
CloseFile(ResultFile);
end;

procedure TFormMain.EquationGaus;                  //������� ��-�� �� ������
var Ar: TMainArray;
    x, y, n: Byte;
    DetX, det, Mn: Real;
begin
try
  Append(ResultFile);
  except Rewrite(ResultFile);
  end;
WriteLn(ResultFile,'-------- ������� ������� �������� ���������. ����� ������ ------');
WriteLn(ResultFile,'-------- '+DateToStr(Date)+' '+TimeToStr(Time));
WriteLn(ResultFile);

for x:=0 to StringGrid1.RowCount-1 do
  begin
  for y:=0 to StringGrid1.ColCount-1 do
    begin
    if y<>0 then
      if StrToFloat(StringGrid1.Cells[y,x])>=0
        then Write(ResultFile,' + ')
        else Write(ResultFile,' - ');
    Write(ResultFile,FloatToStr(Abs(StrToFloat(StringGrid1.Cells[y,x]))),'*x',y+1);
{    Write(ResultFile,StringGrid1.Cells[y,x],'*x',y+1);
    if y<>StringGrid1.ColCount-1 then Write(ResultFile,' + ');    }
    end;
  WriteLn(ResultFile,' = '+StringGrid2.Cells[0,x]);
  end;
WriteLn(ResultFile);

StringGrid3.RowCount:=StringGrid2.RowCount;
StringGrid3.ColCount:=StringGrid2.ColCount;

WriteLn(ResultFile,'[A] =');
WriteLn(ResultFile);

for x:=0 to StringGrid1.RowCount-1 do
  begin
  for y:=0 to StringGrid1.ColCount-1 do
    begin
    Write(ResultFile, StringGrid1.Cells[y,x]+'|');
    end;
  Write(ResultFile, StringGrid2.Cells[0,x]+'|');
  WriteLn(ResultFile);
  end;
WriteLn(ResultFile);

WriteLn(ResultFile,'������ ���:');
WriteLn(ResultFile);

FromGridToArray(StringGrid1,Ar);

for x:=0 to StringGrid1.RowCount-1 do
  begin
  Ar[StringGrid1.RowCount+1,x+1]:=StrToFloat(StringGrid2.Cells[0,x])
  end;

for n:=1 to StringGrid1.RowCount-1 do
  begin
  for x:=n to StringGrid1.RowCount-1 do
    begin
//    WriteLn(ResultFile,'���.',n,);
    Mn:=Ar[n,x+1]/Ar[n,n];
    WriteLn(ResultFile,'( ���.',n,' * ',-Mn:2:2,' ) + ���.',x+1);

//    WriteLn(ResultFile,Ar[n,x+1]:2:2,' / ',Ar[n,n]:2:2,' = ',Mn:2:2);
    for y:=1 to StringGrid1.ColCount+1 do
      begin
      Ar[y,x+1]:=Ar[y,x+1]-Mn*Ar[y,n];
//      WriteLn(ResultFile,'>>  ',Mn:2:2,' * ',Ar[y,n]:2:2,' = ',Ar[y,x+1]:2:2,' ');
      end;
//    WriteLn(ResultFile);
    end;
  WriteLn(ResultFile);

  WriteLn(ResultFile,'[A] ~');
  WriteLn(ResultFile);
  for x:=1 to StringGrid1.RowCount do
    begin
    for y:=1 to StringGrid1.ColCount+1 do
      begin
      Write(ResultFile, Ar[y,x]:2:2,'|');
      end;
    WriteLn(ResultFile);
    end;
  WriteLn(ResultFile);
  end;
//WriteLn(ResultFile);

{WriteLn(ResultFile,'[A] ~');
WriteLn(ResultFile);
for x:=1 to StringGrid1.RowCount do
  begin
  for y:=1 to StringGrid1.ColCount+1 do
    begin
    Write(ResultFile, Ar[y,x]:2:2,'|');
    end;
  WriteLn(ResultFile);
  end;
WriteLn(ResultFile);  }

WriteLn(ResultFile,'�������� ���:');
WriteLn(ResultFile);

for n:=StringGrid1.RowCount downto 2 do
  begin
  for x:=n downto 2 do
    begin
    Mn:=Ar[n,x-1]/Ar[n,n];
    WriteLn(ResultFile,'( ���.',n,' * ',-Mn:2:2,' ) + ���.',x-1);
//    WriteLn(ResultFile,Ar[n,x-1]:2:2,' / ',Ar[n,n]:2:2,' = ',Mn:2:2);
    for y:=StringGrid1.ColCount+1 downto 1 do
      begin
      Ar[y,x-1]:=Ar[y,x-1]-Mn*Ar[y,n];
//      WriteLn(ResultFile,'>>  ',Mn:2:2,' * ',Ar[y,n]:2:2,' = ',Ar[y,x-1]:2:2,' ');
      end;
    end;
  WriteLn(ResultFile);

  WriteLn(ResultFile,'[A] ~');
  WriteLn(ResultFile);
  for x:=1 to StringGrid1.RowCount do
    begin
    for y:=1 to StringGrid1.ColCount+1 do
      begin
      Write(ResultFile, Ar[y,x]:2:2,'|');
      end;
    WriteLn(ResultFile);
    end;
  WriteLn(ResultFile);
  end;

WriteLn(ResultFile,'���������� � ��������� �������:');
WriteLn(ResultFile);

for n:=1 to StringGrid1.RowCount do
  begin
    Mn:=Ar[n,n];
    for y:=1 to StringGrid1.ColCount+1 do
      begin
      Write(ResultFile,Ar[y,n]:2:2,' / ',Mn:2:2,' | ');
      Ar[y,n]:=Ar[y,n]/Mn;
      end;
    WriteLn(ResultFile);
  end;
WriteLn(ResultFile);

WriteLn(ResultFile,'[A] ~');
WriteLn(ResultFile);
for x:=1 to StringGrid1.RowCount do
  begin
  for y:=1 to StringGrid1.ColCount+1 do
    begin
    Write(ResultFile, Ar[y,x]:2:2,'|');
    end;
  WriteLn(ResultFile);
  end;
WriteLn(ResultFile);

for x:=0 to StringGrid1.RowCount-1 do
  begin
  StringGrid3.Cells[0,x]:=FloatToStr(Ar[StringGrid1.RowCount+1,x+1]);
  WriteLn(ResultFile,'x',x+1,' = '+StringGrid3.Cells[0,x]);
  end;
WriteLn(ResultFile);

end;

procedure TFormMain.OpenMatrix(Matrix: TStringGrid; Spin1, Spin2: TSpinEdit; Path: String);
var MFile: TextFile;
    x, y, IndexNewWord, NWord: Integer;
    s, ss: String;
begin
AssignFile(MFile, Path);
Reset(MFile);
ReadLn(MFile,s);
if s<>'Cyber Matrix' then
  begin
  Application.MessageBox(PChar('���� ����� ����������� ������'),PChar(FormMain.Caption),MB_OK+MB_ICONERROR);
  Exit;
  end;
if Matrix=StringGrid1 then
  begin
  if Label4.Caption=']' then Label4Click(Label4);
  end else
  begin
  if Label7.Caption=']' then Label7Click(Label7);
  end;
ReadLn(MFile,s);
for x:=1 to Length(s) do
  begin
  if s[x]='=' then
    begin
    Spin1.Value:=StrToInt(Copy(s,x+1,Length(s)-x));
    end;
  end;
ReadLn(MFile,s);
for x:=1 to Length(s) do
  begin
  if s[x]='=' then
    begin
    Spin2.Value:=StrToInt(Copy(s,x+1,Length(s)-x));
    end;
  end;

for x:=0 to 9 do
  begin
  ReadLn(MFile, s);
  IndexNewWord:=1;
  NWord:=0;
  for y:=1 to Length(s) do
    begin
    if s[y]='|' then
      begin
      ss:=Copy(s,IndexNewWord,y-IndexNewWord);
      Matrix.Cells[NWord,x]:=ss;
      IndexNewWord:=y+1;
      inc(NWord);
      end;
    end;
  end;

CloseFile(MFile);
SelectCellHorizontal(Matrix,-1,0,1);
end;

procedure TFormMain.SaveMatrix(Matrix: TStringGrid; Path: String);
var MFile: TextFile;
    x, y: Byte;
begin
AssignFile(MFile, Path);
Rewrite(MFile);
WriteLn(MFile, 'Cyber Matrix');
WriteLn(MFile, 'M='+IntToStr(Matrix.RowCount));
WriteLn(MFile, 'N='+IntToStr(Matrix.ColCount));

for x:=0 to 9 do
  begin
  for y:=0 to 9 do
    begin
    Write(MFile, Matrix.Cells[y,x]+'|');
    end;
  WriteLn(MFile);
  end;

CloseFile(MFile);
end;

procedure TFormMain.SaveLog(Path: String);
var LFile: TextFile;
    Line: String;
begin
AssignFile(LFile, Path);
Reset(ResultFile);
Rewrite(LFile);

while not eof(ResultFile) do
  begin
  ReadLn(ResultFile,Line);
  WriteLn(LFile,Line);
  end;

CloseFile(ResultFile);
CloseFile(LFile);
end;

procedure TFormMain.CompareMatrix(Source, New: TStringGrid);
var x, y, h, w: Byte;
begin
if Source=StringGrid1 then
  begin
  h:=Spin_m.Value;
  w:=Spin_n.Value;
  if Label7.Caption<>Label4.Caption then Label7Click(Label7);
  end;
if Source=StringGrid2 then
  begin
  h:=Spin_mm.Value;
  w:=Spin_nn.Value;
  if Label4.Caption<>Label7.Caption then Label4Click(Label4);
  end;
if Source=StringGrid3 then
  begin
  h:=Source.RowCount;
  w:=Source.ColCount;
  end;
if New=StringGrid1 then
  begin
  Spin_m.Value:=h;
  Spin_n.Value:=w;
  end else
  begin
  Spin_mm.Value:=h;
  Spin_nn.Value:=w;
  end;
for x:=0 to 9 do
  for y:=0 to 9 do
    begin
    New.Cells[x,y]:=Source.Cells[x,y];
    end;
SelectCellHorizontal(New,-1,0,1);
end;

procedure TFormMain.UnitMatrix(Source: TStringGrid);
var x, y: Byte;
begin
if Source=StringGrid1 then
  begin
  if Label4.Caption='|' then Label4Click(Label4);
  end else
  begin
  if Label7.Caption='|' then Label7Click(Label7);
  end;
for x:=0 to 9 do
  for y:=0 to 9 do
    begin
    if y=x then
      begin
      Source.Cells[x,y]:='1';
      end else
      begin
      Source.Cells[x,y]:='0';
      end;
    end;
SelectCellHorizontal(Source,-1,0,1);
end;

procedure TFormMain.ZeroMatrix(Source: TStringGrid);
var x, y: Byte;
begin
for x:=0 to 9 do
  for y:=0 to 9 do
    begin
    Source.Cells[x,y]:='0';
    end;
SelectCellHorizontal(Source,-1,0,1);
end;

procedure TFormMain.Switch (Source: TStringGrid);
var x, y, h, w: Byte;
    Temp: String;
begin
h:=Spin_m.Value;
Spin_m.Value:=Spin_mm.Value;
Spin_mm.Value:=h;
if Label7.Caption<>Label4.Caption then
  begin
  Label7Click(Label7);
  Label4Click(Label4);
  end;
w:=Spin_n.Value;
Spin_n.Value:=Spin_nn.Value;
Spin_nn.Value:=w;
for x:=0 to 9 do
  for y:=0 to 9 do
    begin
    Temp:=StringGrid2.Cells[x,y];
    StringGrid2.Cells[x,y]:=StringGrid1.Cells[x,y];
    StringGrid1.Cells[x,y]:=Temp;
    end;
end;

procedure TFormMain.SelectCellHorizontal(SGrid: TStringGrid; I, J, Direction: Integer);
var Temp: Boolean;
    SRect: TGridRect;
begin
if Direction>=0 then
  begin
  if I<>SGrid.ColCount-1 then
  I:=I+Direction
  else
  if J<>SGrid.RowCount-1 then
    begin
    I:=0;
    J:=J+Direction;
    end else
    begin
    I:=0;
    J:=0;
    end;
  end else
  begin
  if I<>0 then
    I:=I+Direction
    else
    if J<>0 then
      begin
      I:=SGrid.ColCount-1;
      J:=J+Direction;
      end else
      begin
      I:=SGrid.ColCount-1;
      J:=SGrid.RowCount-1;
      end;
  end;
SRect.Top:=J;
SRect.Left:=I;
SRect.Bottom:=J;
SRect.Right:=I;
SGrid.Selection:=SRect;
SGrid.OnSelectCell(SGrid, I, J, Temp);
end;

procedure TFormMain.SelectCellVertical(SGrid: TStringGrid; I, J, Direction: integer);
var Temp: Boolean;
    SRect: TGridRect;
begin
if Direction>=0 then
  begin
  if J<>SGrid.RowCount-1 then
    J:=J+Direction
    else
    if I<>SGrid.ColCount-1 then
      begin
      J:=0;
      I:=I+Direction;
      end else
      begin
      I:=0;
      J:=0;
      end;
  end else
  begin
  if J<>0 then
    J:=J+Direction
    else
    if I<>0 then
      begin
      J:=SGrid.RowCount-1;
      I:=I+Direction;
      end else
      begin
      I:=SGrid.ColCount-1;
      J:=SGrid.RowCount-1;
      end;
  end;
SRect.Top:=J;
SRect.Left:=I;
SRect.Bottom:=J;
SRect.Right:=I;
SGrid.Selection:=SRect;
SGrid.OnSelectCell(SGrid, I, J, Temp);
end;

procedure TFormMain.StringGrid1SelectCell(Sender: TObject; ACol,
  ARow: Integer; var CanSelect: Boolean);
var x, y: Byte;
begin
cI1:=ACol;
cJ1:=ARow;
for x:=0 to 9 do
  for y:=0 to 9 do
    begin
    if StringGrid1.Cells[x,y]='' then StringGrid1.Cells[x,y]:='0';
    end;
SpinEdit1.Value:=cJ1+1;
SpinEdit3.Value:=cI1+1;
Edit1.Text:=StringGrid1.Cells[ACol,ARow];
Edit1.Left:=StringGrid1.Left+(StringGrid1.DefaultColWidth+StringGrid1.GridLineWidth)*ACol+StringGrid1.GridLineWidth;
Edit1.Top:=StringGrid1.Top+(StringGrid1.DefaultRowHeight+StringGrid1.GridLineWidth)*ARow+StringGrid1.GridLineWidth;
Edit1.SelectAll;
Edit1.SetFocus;
end;

procedure TFormMain.StringGrid2SelectCell(Sender: TObject; ACol,
  ARow: Integer; var CanSelect: Boolean);
var x, y: Byte;
begin
cI2:=ACol;
cJ2:=ARow;
for x:=0 to 9 do
  for y:=0 to 9 do
    begin
    if StringGrid2.Cells[x,y]='' then StringGrid2.Cells[x,y]:='0';
    end;
SpinEdit2.Value:=cJ2+1;
SpinEdit4.Value:=cI2+1;
Edit2.Text:=StringGrid2.Cells[ACol,ARow];
Edit2.Left:=StringGrid2.Left+(StringGrid2.DefaultColWidth+StringGrid2.GridLineWidth)*ACol+StringGrid2.GridLineWidth;
Edit2.Top:=StringGrid2.Top+(StringGrid2.DefaultRowHeight+StringGrid2.GridLineWidth)*ARow+StringGrid2.GridLineWidth;
Edit2.SelectAll;
Edit2.SetFocus;
end;

procedure TFormMain.StringGrid3SelectCell(Sender: TObject; ACol,
  ARow: Integer; var CanSelect: Boolean);
begin
cI3:=ACol;
cJ3:=ARow;
Edit3.Text:=StringGrid3.Cells[ACol,ARow];
Edit3.Left:=StringGrid3.Left+(StringGrid3.DefaultColWidth+StringGrid3.GridLineWidth)*ACol+StringGrid3.GridLineWidth;
Edit3.Top:=StringGrid3.Top+(StringGrid3.DefaultRowHeight+StringGrid3.GridLineWidth)*ARow+StringGrid3.GridLineWidth;
Edit3.SelectAll;
Edit3.SetFocus;
end;

procedure TFormMain.Edit1Change(Sender: TObject);
begin
StringGrid1.Cells[cI1,cJ1]:=Edit1.Text;
end;

procedure TFormMain.Edit2Change(Sender: TObject);
begin
StringGrid2.Cells[cI2,cJ2]:=Edit2.Text;
end;

procedure TFormMain.Edit1Click(Sender: TObject);
begin
Edit1.SelectAll;
end;

procedure TFormMain.Edit2Click(Sender: TObject);
begin
Edit2.SelectAll;
end;

procedure TFormMain.Edit3Click(Sender: TObject);
begin
Edit3.SelectAll;
end;

procedure TFormMain.Label4Click(Sender: TObject);
begin
If Label4.Caption='|' then
  begin
  Label4.Caption:=']';
  Spin_m.Enabled:=False;
  Spin_m.Value:=Spin_n.Value;
  end else
  begin
  Label4.Caption:='|';
  Spin_m.Enabled:=True;;
  end;
end;

procedure TFormMain.FormCreate(Sender: TObject);
var x, y: Byte;
begin
cfg:=Tinifile.Create(ExtractFilePath(ParamStr(0))+'Matrix.ini');
Left:=cfg.ReadInteger('mainform','left',267);
Top:=cfg.ReadInteger('mainform','top',131);
N23.Checked:=cfg.ReadBool('program','clear_log',true);
PageControl1.TabIndex:=cfg.ReadInteger('program','tab_index1',0);
PageControl2.TabIndex:=cfg.ReadInteger('program','tab_index2',0);

for x:=0 to 9 do
  for y:=0 to 9 do
    begin
    StringGrid1.Cells[y,x]:='0';
    StringGrid2.Cells[y,x]:='0';
    StringGrid3.Cells[y,x]:='0';
    end;
cI1:=0;
cI2:=0;
cI3:=0;
cJ1:=0;
cJ2:=0;
cJ3:=0;
AssignFile(ResultFile, ExtractFilePath(ParamStr(0))+'Result.txt');
if N23.Checked then
  begin
  Rewrite(ResultFile);
  CloseFile(ResultFile);
  end;
end;

procedure TFormMain.FormClose(Sender: TObject; var Action: TCloseAction);
begin
cfg.WriteInteger('resultform','width',FormResult.Width);
cfg.WriteInteger('resultform','height',FormResult.Height);
cfg.WriteInteger('mainform','left',Left);
cfg.WriteInteger('mainform','top',Top);
cfg.WriteBool('program','clear_log',N23.Checked);
cfg.WriteInteger('program','tab_index1',PageControl1.TabIndex);
cfg.WriteInteger('program','tab_index2',PageControl2.TabIndex);
end;

procedure TFormMain.Spin_mChange(Sender: TObject);
begin
StringGrid1.RowCount:=Spin_m.Value;
if Spin_n.Value<>Spin_m.Value then
  begin
  Button8.Enabled:=False;
  Button1.Enabled:=False;
  Aij1.Enabled:=False;
  detA1.Enabled:=False;
  end else
  begin
  Button8.Enabled:=True;
  Button1.Enabled:=True;
  Aij1.Enabled:=True;
  detA1.Enabled:=True;
  end;
if Spin_m.Value=1 then
  begin
  SpinEdit1.Value:=1;
  SpinEdit1.Enabled:=False;
  end else
  begin
  SpinEdit1.Enabled:=True;
  SpinEdit1.MaxValue:=Spin_m.Value;
  SpinEdit5.MaxValue:=Spin_m.Value;
  if SpinEdit1.Value>SpinEdit1.MaxValue then SpinEdit1.Value:=SpinEdit1.MaxValue;
  if SpinEdit5.Value>SpinEdit5.MaxValue then SpinEdit5.Value:=SpinEdit5.MaxValue;

  FormAdd.SpinEdit5.MaxValue:=Spin_m.Value;
  FormAdd.SpinEdit7.MaxValue:=Spin_m.Value;
  if FormAdd.SpinEdit5.Value>FormAdd.SpinEdit5.MaxValue then FormAdd.SpinEdit5.Value:=FormAdd.SpinEdit5.MaxValue;
  if FormAdd.SpinEdit7.Value>FormAdd.SpinEdit7.MaxValue then FormAdd.SpinEdit7.Value:=FormAdd.SpinEdit7.MaxValue;
  end;
end;

procedure TFormMain.Spin_nChange(Sender: TObject);
begin
StringGrid1.ColCount:=Spin_n.Value;
if Spin_n.Value<>Spin_m.Value then
  begin
  Button8.Enabled:=False;
  Button1.Enabled:=False;
  Aij1.Enabled:=False;
  detA1.Enabled:=False;
  end else
  begin
  Button8.Enabled:=True;
  Button1.Enabled:=True;
  Aij1.Enabled:=True;
  detA1.Enabled:=True;
  end;
if Spin_n.Value=1 then
  begin
  SpinEdit3.Value:=1;
  SpinEdit3.Enabled:=False;
  end else
  begin
  SpinEdit3.Enabled:=True;
  SpinEdit3.MaxValue:=Spin_n.Value;
  SpinEdit6.MaxValue:=Spin_n.Value;
  if SpinEdit3.Value>SpinEdit3.MaxValue then SpinEdit3.Value:=SpinEdit3.MaxValue;
  if SpinEdit6.Value>SpinEdit6.MaxValue then SpinEdit6.Value:=SpinEdit6.MaxValue;

  FormAdd.SpinEdit6.MaxValue:=Spin_n.Value;
  FormAdd.SpinEdit8.MaxValue:=Spin_n.Value;
  if FormAdd.SpinEdit6.Value>FormAdd.SpinEdit6.MaxValue then FormAdd.SpinEdit6.Value:=FormAdd.SpinEdit6.MaxValue;
  if FormAdd.SpinEdit8.Value>FormAdd.SpinEdit8.MaxValue then FormAdd.SpinEdit8.Value:=FormAdd.SpinEdit8.MaxValue;
  end;
If (Label4.Caption=']') then
  begin
  Spin_m.Value:=Spin_n.Value;
  end;
end;

procedure TFormMain.Spin_mmChange(Sender: TObject);
begin
StringGrid2.RowCount:=Spin_mm.Value;
if Spin_nn.Value<>Spin_mm.Value then
  begin
  Button9.Enabled:=False;
  Button5.Enabled:=False;
  Bij1.Enabled:=False;
  detB1.Enabled:=False;
  end else
  begin
  Button9.Enabled:=True;
  Button5.Enabled:=True;
  Bij1.Enabled:=True;
  detB1.Enabled:=True;
  end;
if Spin_mm.Value=1 then
  begin
  SpinEdit2.Value:=1;
  SpinEdit2.Enabled:=False;
  end else
  begin
  SpinEdit2.Enabled:=True;
  SpinEdit7.MaxValue:=Spin_mm.Value;
  SpinEdit2.MaxValue:=Spin_mm.Value;
  if SpinEdit2.Value>SpinEdit2.MaxValue then SpinEdit2.Value:=SpinEdit2.MaxValue;
  if SpinEdit7.Value>SpinEdit7.MaxValue then SpinEdit7.Value:=SpinEdit7.MaxValue;

  FormAdd.SpinEdit5.MaxValue:=Spin_mm.Value;
  FormAdd.SpinEdit7.MaxValue:=Spin_mm.Value;
  if FormAdd.SpinEdit5.Value>FormAdd.SpinEdit5.MaxValue then FormAdd.SpinEdit5.Value:=FormAdd.SpinEdit5.MaxValue;
  if FormAdd.SpinEdit7.Value>FormAdd.SpinEdit7.MaxValue then FormAdd.SpinEdit7.Value:=FormAdd.SpinEdit7.MaxValue;
  end;
end;

procedure TFormMain.Spin_nnChange(Sender: TObject);
begin
StringGrid2.ColCount:=Spin_nn.Value;
if Spin_nn.Value<>Spin_mm.Value then
  begin
  Button9.Enabled:=False;
  Button5.Enabled:=False;
  Bij1.Enabled:=False;
  detB1.Enabled:=False;
  end else
  begin
  Button9.Enabled:=True;
  Button5.Enabled:=True;
  Bij1.Enabled:=True;
  detB1.Enabled:=True;
  end;
if Spin_nn.Value=1 then
  begin
  SpinEdit4.Value:=1;
  SpinEdit4.Enabled:=False;
  end else
  begin
  SpinEdit4.Enabled:=True;
  SpinEdit4.MaxValue:=Spin_nn.Value;
  SpinEdit8.MaxValue:=Spin_nn.Value;
  if SpinEdit4.Value>SpinEdit4.MaxValue then SpinEdit4.Value:=SpinEdit4.MaxValue;
  if SpinEdit8.Value>SpinEdit8.MaxValue then SpinEdit8.Value:=SpinEdit8.MaxValue;

  FormAdd.SpinEdit6.MaxValue:=Spin_nn.Value;
  FormAdd.SpinEdit8.MaxValue:=Spin_nn.Value;
  if FormAdd.SpinEdit6.Value>FormAdd.SpinEdit6.MaxValue then FormAdd.SpinEdit6.Value:=FormAdd.SpinEdit6.MaxValue;
  if FormAdd.SpinEdit8.Value>FormAdd.SpinEdit8.MaxValue then FormAdd.SpinEdit8.Value:=FormAdd.SpinEdit8.MaxValue;
  end;
if (Label7.Caption=']') then
  begin
  Spin_mm.Value:=Spin_nn.Value;
  end;
end;

procedure TFormMain.Label7Click(Sender: TObject);
begin
If Label7.Caption='|' then
  begin
  Label7.Caption:=']';
  Spin_mm.Enabled:=False;
  Spin_mm.Value:=Spin_nn.Value;
  end else
  begin
  Label7.Caption:='|';
  Spin_mm.Enabled:=True;;
  end;
end;

procedure TFormMain.Edit1KeyPress(Sender: TObject; var Key: Char);
begin
if not((Key>=#48)and(Key<=#57)or(Key=#44)or(Key=#45)or(Key=#8)) then Key:=#0;
end;

procedure TFormMain.Edit2KeyPress(Sender: TObject; var Key: Char);
begin
if not((Key>=#48)and(Key<=#57)or(Key=#44)or(Key=#45)or(Key=#8)) then Key:=#0;
end;

procedure TFormMain.Edit1KeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var n: Integer;
begin
if Shift = [ssShift]
  then n:=-1 else n:=1;
case Key of
  9:
    begin
    SelectCellHorizontal(StringGrid1, cI1, cJ1, n);
    Edit1.SelectAll;
    end;
  13:
    begin
    SelectCellVertical(StringGrid1, cI1, cJ1, n);
    Edit1.SelectAll;
    end;
  end;
end;

procedure TFormMain.Edit2KeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var n: Integer;
begin
if Shift = [ssShift]
  then n:=-1 else n:=1;
case Key of
  9:
    begin
    SelectCellHorizontal(StringGrid2, cI2, cJ2, n);
    Edit2.SelectAll;
    end;
  13:
    begin
    SelectCellVertical(StringGrid2, cI2, cJ2, n);
    Edit2.SelectAll;
    end;
  end;
end;

procedure TFormMain.Edit3KeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var n: Integer;
begin
if Shift = [ssShift]
  then n:=-1 else n:=1;
case Key of
  9:
    begin
    SelectCellHorizontal(StringGrid3, cI3, cJ3, n);
    Edit3.SelectAll;
    end;
  13:
    begin
    SelectCellVertical(StringGrid3, cI3, cJ3, n);
    Edit3.SelectAll;
    end;
  end;
end;

procedure TFormMain.TabSheet6Show(Sender: TObject);
begin
FormAdd.Label2.Visible:=True;
FormAdd.Label2.Caption:='[ A ]';
FormAdd.SpinEdit5.MaxValue:=Spin_m.Value;
FormAdd.SpinEdit7.MaxValue:=Spin_m.Value;
if FormAdd.SpinEdit5.Value>FormAdd.SpinEdit5.MaxValue then FormAdd.SpinEdit5.Value:=FormAdd.SpinEdit5.MaxValue;
if FormAdd.SpinEdit7.Value>FormAdd.SpinEdit7.MaxValue then FormAdd.SpinEdit7.Value:=FormAdd.SpinEdit7.MaxValue;
FormAdd.SpinEdit6.MaxValue:=Spin_n.Value;
FormAdd.SpinEdit8.MaxValue:=Spin_n.Value;
if FormAdd.SpinEdit6.Value>FormAdd.SpinEdit6.MaxValue then FormAdd.SpinEdit6.Value:=FormAdd.SpinEdit6.MaxValue;
if FormAdd.SpinEdit8.Value>FormAdd.SpinEdit8.MaxValue then FormAdd.SpinEdit8.Value:=FormAdd.SpinEdit8.MaxValue;

FormRandom.Button2.Enabled:=True;
FormRandom.Label2.Caption:='[ A ]';
FormRandom.Label2.Visible:=True;

Edit1.Text:=StringGrid1.Cells[cI1,cJ1];
Edit1.SetFocus;
end;

procedure TFormMain.TabSheet7Show(Sender: TObject);
begin
FormAdd.Label2.Visible:=True;
FormAdd.Label2.Caption:='[ B ]';
FormAdd.SpinEdit5.MaxValue:=Spin_mm.Value;
FormAdd.SpinEdit7.MaxValue:=Spin_mm.Value;
if FormAdd.SpinEdit5.Value>FormAdd.SpinEdit5.MaxValue then FormAdd.SpinEdit5.Value:=FormAdd.SpinEdit5.MaxValue;
if FormAdd.SpinEdit7.Value>FormAdd.SpinEdit7.MaxValue then FormAdd.SpinEdit7.Value:=FormAdd.SpinEdit7.MaxValue;
FormAdd.SpinEdit6.MaxValue:=Spin_nn.Value;
FormAdd.SpinEdit8.MaxValue:=Spin_nn.Value;
if FormAdd.SpinEdit6.Value>FormAdd.SpinEdit6.MaxValue then FormAdd.SpinEdit6.Value:=FormAdd.SpinEdit6.MaxValue;
if FormAdd.SpinEdit8.Value>FormAdd.SpinEdit8.MaxValue then FormAdd.SpinEdit8.Value:=FormAdd.SpinEdit8.MaxValue;

FormRandom.Button2.Enabled:=True;
FormRandom.Label2.Caption:='[ B ]';
FormRandom.Label2.Visible:=True;

Edit2.Text:=StringGrid2.Cells[cI2,cJ2];
Edit2.SetFocus;
end;

procedure TFormMain.TabSheet8Show(Sender: TObject);
begin
FormAdd.Label2.Visible:=False;

FormRandom.Button2.Enabled:=False;
FormRandom.Label2.Visible:=False;

Edit3.Text:=StringGrid3.Cells[cI3,cJ3];
Edit3.SetFocus;
end;

//������

procedure TFormMain.Button8Click(Sender: TObject);
begin
if StringGrid1.RowCount=StringGrid1.ColCount then
  begin
  Edit7.Text:=FloatToStr(Complement(StringGrid1,SpinEdit1.Value,SpinEdit3.Value,'A'));
  FormResult.Button1Click(Button1);
  end else
  begin
  Edit7.Text:='';
  Application.MessageBox(PChar('���������� ����� �� ��������� � ����������� �������� �������'),PChar(FormMain.Caption),MB_OK+MB_ICONERROR);
  end;
end;

procedure TFormMain.Button9Click(Sender: TObject);
begin
if StringGrid2.RowCount=StringGrid2.ColCount then
  begin
  Edit6.Text:=FloatToStr(Complement(StringGrid2,SpinEdit2.Value,SpinEdit4.Value,'B'));
  FormResult.Button1Click(Button1);
  end else
  begin
  Edit6.Text:='';
  Application.MessageBox(PChar('���������� ����� �� ��������� � ����������� �������� �������'),PChar(FormMain.Caption),MB_OK+MB_ICONERROR);
  end;
end;

procedure TFormMain.Button1Click(Sender: TObject);
begin
if StringGrid1.RowCount=StringGrid1.ColCount then
  begin
  Edit4.Text:=FloatToStr(DetA(StringGrid1, SpinEdit5.Value, SpinEdit6.Value,'A'));
  FormResult.Button1Click(Button1);
  end else
  begin
  Edit4.Text:='';
  Application.MessageBox(PChar('���������� ����� �� ��������� � ����������� �������� �������'),PChar(FormMain.Caption),MB_OK+MB_ICONERROR);
  end;
end;

procedure TFormMain.Button5Click(Sender: TObject);
begin
if StringGrid2.RowCount=StringGrid2.ColCount then
  begin
  Edit5.Text:=FloatToStr(DetA(StringGrid2, SpinEdit7.Value, SpinEdit8.Value,'B'));
  FormResult.Button1Click(Button1);
  end else
  begin
  Edit5.Text:='';
  Application.MessageBox(PChar('���������� ����� �� ��������� � ����������� �������� �������'),PChar(FormMain.Caption),MB_OK+MB_ICONERROR);
  end;
end;

procedure TFormMain.Button3Click(Sender: TObject);
begin
if StringGrid1.ColCount=StringGrid2.RowCount then
  begin
  Multiple(StringGrid1,StringGrid2,'A','B');
  FormResult.Button1Click(Button1);
  end
  else begin
  Application.MessageBox(PChar('���������� �������� ������� A �� ��������� � ����������� ����� ������� B'),PChar(FormMain.Caption),MB_OK+MB_ICONERROR);
  end;
end;

procedure TFormMain.Button4Click(Sender: TObject);
begin
if StringGrid2.ColCount=StringGrid1.RowCount then
  begin
  Multiple(StringGrid2,StringGrid1,'B','A');
  FormResult.Button1Click(Button1);
  end
  else begin
  Application.MessageBox(PChar('���������� �������� ������� B �� ��������� � ����������� ����� ������� A'),PChar(FormMain.Caption),MB_OK+MB_ICONERROR);
  end;
end;

procedure TFormMain.Button2Click(Sender: TObject);
begin
if StringGrid1.ColCount=StringGrid2.ColCount then
  begin
  if StringGrid1.RowCount=StringGrid2.RowCount then
    begin
    Summ;
    FormResult.Button1Click(Button1);
    end else
    begin
    Application.MessageBox(PChar('���������� ����� ������� A �� ��������� � ����������� ����� ������� B'),PChar(FormMain.Caption),MB_OK+MB_ICONERROR);
    end;
  end else
  begin
  Application.MessageBox(PChar('���������� �������� ������� A �� ��������� � ����������� �������� ������� B'),PChar(FormMain.Caption),MB_OK+MB_ICONERROR);
  end;
end;

procedure TFormMain.Button6Click(Sender: TObject);
begin
if StringGrid1.ColCount=StringGrid1.RowCount then
  begin
  Reverse(StringGrid1,'A');
  FormResult.Button1Click(Button1);
  end else
  begin
  Application.MessageBox(PChar('���������� ����� �� ��������� � ����������� �������� ������� A'),PChar(FormMain.Caption),MB_OK+MB_ICONERROR);
  end;
end;

procedure TFormMain.Button7Click(Sender: TObject);
begin
if StringGrid2.ColCount=StringGrid2.RowCount then
  begin
  Reverse(StringGrid2,'B');
  FormResult.Button1Click(Button1);
  end else
  begin
  Application.MessageBox(PChar('���������� ����� �� ��������� � ����������� �������� ������� B'),PChar(FormMain.Caption),MB_OK+MB_ICONERROR);
  end;
end;

procedure TFormMain.Button10Click(Sender: TObject);
begin
if StringGrid1.ColCount=StringGrid1.RowCount then
  begin
  TranspS(StringGrid1,'A');
  FormResult.Button1Click(Button1);
  end else
  begin
  Application.MessageBox(PChar('���������� ����� �� ��������� � ����������� �������� ������� A'),PChar(FormMain.Caption),MB_OK+MB_ICONERROR);
  end;
end;

procedure TFormMain.Button11Click(Sender: TObject);
begin
if StringGrid2.ColCount=StringGrid2.RowCount then
  begin
  TranspS(StringGrid2,'B');
  FormResult.Button1Click(Button1);
  end else
  begin
  Application.MessageBox(PChar('���������� ����� �� ��������� � ����������� �������� ������� B'),PChar(FormMain.Caption),MB_OK+MB_ICONERROR);
  end;
end;

procedure TFormMain.Button12Click(Sender: TObject);
begin
if StringGrid1.ColCount=StringGrid1.RowCount then
  begin
  if StringGrid1.ColCount=StringGrid2.RowCount then
    begin
    if StringGrid2.ColCount=1 then
      begin
      Equation;
      FormResult.Button1Click(Button1);
      end else
      begin
      Application.MessageBox(PChar('���������� �������� ������� B �� ����� 1'),PChar(FormMain.Caption),MB_OK+MB_ICONERROR);
      PageControl2.Pages[1].Show;
      SelectCellHorizontal(StringGrid2,-1,0,1);
      end;
    end
    else begin
    Application.MessageBox(PChar('���������� ����� ������� B �� ��������� � ����������� ����� (��������) ������� A'),PChar(FormMain.Caption),MB_OK+MB_ICONERROR);
    PageControl2.Pages[1].Show;
    SelectCellHorizontal(StringGrid2,-1,0,1);
    end;
  end else
  begin
  Application.MessageBox(PChar('���������� ����� �� ��������� � ����������� �������� ������� A'),PChar(FormMain.Caption),MB_OK+MB_ICONERROR);
  PageControl2.Pages[0].Show;
  SelectCellHorizontal(StringGrid1,-1,0,1);
  end;
end;

procedure TFormMain.Button14Click(Sender: TObject);
begin
if StringGrid1.ColCount=StringGrid1.RowCount then
  begin
  if StringGrid1.ColCount=StringGrid2.RowCount then
    begin
    if StringGrid2.ColCount=1 then
      begin
      try
        EquationGaus;
        PageControl2.Pages[2].Show;
        SelectCellHorizontal(StringGrid3,-1,0,1);
        except
          try WriteLn(ResultFile,'������!');
              WriteLn(ResultFile);
            except
            Append(ResultFile);
            WriteLn(ResultFile,'������!');
            WriteLn(ResultFile);
            end;
          Application.MessageBox(PChar('��������� ������ ��� ��������'),PChar(FormMain.Caption),MB_ok+MB_IconError);
          PageControl2.Pages[0].Show;
          SelectCellHorizontal(StringGrid1,-1,0,1);
        end;
      CloseFile(ResultFile);
      FormResult.Button1Click(Button1);
      end else
      begin
      Application.MessageBox(PChar('���������� �������� ������� B �� ����� 1'),PChar(FormMain.Caption),MB_OK+MB_ICONERROR);
      PageControl2.Pages[1].Show;
      SelectCellHorizontal(StringGrid2,-1,0,1);
      end;
    end
    else begin
    Application.MessageBox(PChar('���������� ����� ������� B �� ��������� � ����������� ����� (��������) ������� A'),PChar(FormMain.Caption),MB_OK+MB_ICONERROR);
    PageControl2.Pages[1].Show;
    SelectCellHorizontal(StringGrid2,-1,0,1);
    end;
  end else
  begin
  Application.MessageBox(PChar('���������� ����� �� ��������� � ����������� �������� ������� A'),PChar(FormMain.Caption),MB_OK+MB_ICONERROR);
  PageControl2.Pages[0].Show;
  SelectCellHorizontal(StringGrid1,-1,0,1);
  end;
end;

procedure TFormMain.Button13Click(Sender: TObject);
begin
if StringGrid1.ColCount=StringGrid1.RowCount then
  begin
  if StringGrid1.ColCount=StringGrid2.RowCount then
    begin
    if StringGrid2.ColCount=1 then
      begin
      EquationCramer;
      FormResult.Button1Click(Button1);
      end else
      begin
      Application.MessageBox(PChar('���������� �������� ������� B �� ����� 1'),PChar(FormMain.Caption),MB_OK+MB_ICONERROR);
      PageControl2.Pages[1].Show;
      SelectCellHorizontal(StringGrid2,-1,0,1);
      end;
    end
    else begin
    Application.MessageBox(PChar('���������� ����� ������� B �� ��������� � ����������� ����� (��������) ������� A'),PChar(FormMain.Caption),MB_OK+MB_ICONERROR);
    PageControl2.Pages[1].Show;
    SelectCellHorizontal(StringGrid2,-1,0,1);
    end;
  end else
  begin
  Application.MessageBox(PChar('���������� ����� �� ��������� � ����������� �������� ������� A'),PChar(FormMain.Caption),MB_OK+MB_ICONERROR);
  PageControl2.Pages[0].Show;
  SelectCellHorizontal(StringGrid1,-1,0,1);
  end;
end;

procedure TFormMain.Button16Click(Sender: TObject);
begin
Transpon(StringGrid1,'A');
FormResult.Button1Click(Button1);
end;

procedure TFormMain.Button17Click(Sender: TObject);
begin
Transpon(StringGrid2,'B');
FormResult.Button1Click(Button1);
end;

//Menu ����

procedure TFormMain.N3Click(Sender: TObject);                    //��������� [�]...
begin
OpenDialog1.Title:='��������� [A]';
if OpenDialog1.Execute then
  begin
  PageControl2.Pages[0].Show;
  OpenMatrix(StringGrid1, Spin_m, Spin_n, OpenDialog1.FileName);
  end;
end;

procedure TFormMain.B2Click(Sender: TObject);                   //��������� [B]...
begin
OpenDialog1.Title:='��������� [B]';
if OpenDialog1.Execute then
  begin
  PageControl2.Pages[1].Show;
  OpenMatrix(StringGrid2, Spin_mm, Spin_nn,OpenDialog1.FileName);
  end;
end;

procedure TFormMain.A1Click(Sender: TObject);                 //��������� [A] ���...
begin
SaveDialog1.Title:='��������� [A] ���';
if SaveDialog1.Execute then
  SaveMatrix(StringGrid1, SaveDialog1.FileName);
end;

procedure TFormMain.B1Click(Sender: TObject);                 //��������� [B] ���...
begin
SaveDialog1.Title:='��������� [B] ���';
if SaveDialog1.Execute then
  SaveMatrix(StringGrid2, SaveDialog1.FileName);
end;

procedure TFormMain.C1Click(Sender: TObject);                 //��������� [C] ���...
begin
SaveDialog1.Title:='��������� [C] ���';
if SaveDialog1.Execute then
  SaveMatrix(StringGrid3, SaveDialog1.FileName);
end;

procedure TFormMain.N4Click(Sender: TObject);                 //�����
begin
Close;
end;

//Menu ������� A

procedure TFormMain.AE1Click(Sender: TObject);                //A:=E
begin
PageControl2.Pages[0].Show;
UnitMatrix(StringGrid1);
end;

procedure TFormMain.A01Click(Sender: TObject);                //A:=0
begin
PageControl2.Pages[0].Show;
ZeroMatrix(StringGrid1);
end;

procedure TFormMain.AB1Click(Sender: TObject);                //A:=B
begin
PageControl2.Pages[0].Show;
CompareMatrix(StringGrid2,StringGrid1);
end;

procedure TFormMain.BA1Click(Sender: TObject);                 //A:=C
begin
PageControl2.Pages[0].Show;
CompareMatrix(StringGrid3,StringGrid1);
end;

procedure TFormMain.N33Click(Sender: TObject);//�������� ��� �������� (���������)
begin
PageControl2.Pages[0].Show;
FormAdd.Label2.Visible:=True;
FormAdd.Label2.Caption:='[ A ]';
FormAdd.SpinEdit5.MaxValue:=Spin_m.Value;
FormAdd.SpinEdit7.MaxValue:=Spin_m.Value;
if FormAdd.SpinEdit5.Value>FormAdd.SpinEdit5.MaxValue then FormAdd.SpinEdit5.Value:=FormAdd.SpinEdit5.MaxValue;
if FormAdd.SpinEdit7.Value>FormAdd.SpinEdit7.MaxValue then FormAdd.SpinEdit7.Value:=FormAdd.SpinEdit7.MaxValue;
FormAdd.SpinEdit6.MaxValue:=Spin_n.Value;
FormAdd.SpinEdit8.MaxValue:=Spin_n.Value;
if FormAdd.SpinEdit6.Value>FormAdd.SpinEdit6.MaxValue then FormAdd.SpinEdit6.Value:=FormAdd.SpinEdit6.MaxValue;
if FormAdd.SpinEdit8.Value>FormAdd.SpinEdit8.MaxValue then FormAdd.SpinEdit8.Value:=FormAdd.SpinEdit8.MaxValue;

FormAdd.Show;
end;

procedure TFormMain.N38Click(Sender: TObject);
begin
PageControl2.Pages[0].Show;
FormRandom.Button2.Enabled:=True;
FormRandom.Label2.Caption:='[ A ]';
FormRandom.Label2.Visible:=True;
FormRandom.Show;
end;

procedure TFormMain.AB2Click(Sender: TObject);                 //A<->B
begin
PageControl2.Pages[1].Show;
SelectCellHorizontal(StringGrid2,-1,0,1);
PageControl2.Pages[0].Show;
SelectCellHorizontal(StringGrid1,-1,0,1);
Switch(StringGrid1);
end;

//Menu ������� B

procedure TFormMain.BE1Click(Sender: TObject);                //B:=E
begin
PageControl2.Pages[1].Show;
UnitMatrix(StringGrid2);
end;

procedure TFormMain.B01Click(Sender: TObject);               //B:=0
begin
PageControl2.Pages[1].Show;
ZeroMatrix(StringGrid2);
end;

procedure TFormMain.BA2Click(Sender: TObject);               //B:=A
begin
PageControl2.Pages[1].Show;
CompareMatrix(StringGrid1,StringGrid2);
end;

procedure TFormMain.BC1Click(Sender: TObject);                //B:=C
begin
PageControl2.Pages[1].Show;
CompareMatrix(StringGrid3,StringGrid2);
end;

procedure TFormMain.N34Click(Sender: TObject);//�������� ��� �������� (���������)
begin
PageControl2.Pages[1].Show;
FormAdd.Label2.Visible:=True;
FormAdd.Label2.Caption:='[ B ]';
FormAdd.SpinEdit5.MaxValue:=Spin_mm.Value;
FormAdd.SpinEdit7.MaxValue:=Spin_mm.Value;
if FormAdd.SpinEdit5.Value>FormAdd.SpinEdit5.MaxValue then FormAdd.SpinEdit5.Value:=FormAdd.SpinEdit5.MaxValue;
if FormAdd.SpinEdit7.Value>FormAdd.SpinEdit7.MaxValue then FormAdd.SpinEdit7.Value:=FormAdd.SpinEdit7.MaxValue;
FormAdd.SpinEdit6.MaxValue:=Spin_nn.Value;
FormAdd.SpinEdit8.MaxValue:=Spin_nn.Value;
if FormAdd.SpinEdit6.Value>FormAdd.SpinEdit6.MaxValue then FormAdd.SpinEdit6.Value:=FormAdd.SpinEdit6.MaxValue;
if FormAdd.SpinEdit8.Value>FormAdd.SpinEdit8.MaxValue then FormAdd.SpinEdit8.Value:=FormAdd.SpinEdit8.MaxValue;

FormAdd.Show;
end;

procedure TFormMain.N40Click(Sender: TObject);
begin
PageControl2.Pages[1].Show;
FormRandom.Button2.Enabled:=True;
FormRandom.Label2.Caption:='[ B ]';
FormRandom.Label2.Visible:=True;
FormRandom.Show;
end;

procedure TFormMain.BA3Click(Sender: TObject);                //B<->A
begin
PageControl2.Pages[0].Show;
SelectCellHorizontal(StringGrid1,-1,0,1);
PageControl2.Pages[1].Show;
SelectCellHorizontal(StringGrid2,-1,0,1);
Switch(StringGrid2);
end;

//Menu ��������

procedure TFormMain.Aij1Click(Sender: TObject);               //Aij
begin
PageControl1.Pages[0].Show;
Button8Click(Button8);
end;

procedure TFormMain.Bij1Click(Sender: TObject);               //Bij
begin
PageControl1.Pages[0].Show;
Button9Click(Button9);
end;

procedure TFormMain.detA1Click(Sender: TObject);              //det(A)
begin
PageControl1.Pages[1].Show;
PageControl2.Pages[0].Show;
Button1Click(Button1);
end;

procedure TFormMain.detB1Click(Sender: TObject);              //det(B)
begin
PageControl1.Pages[1].Show;
PageControl2.Pages[1].Show;
Button5Click(Button5);
end;

procedure TFormMain.CAB2Click(Sender: TObject);                //C:=A*B
begin
PageControl1.Pages[2].Show;
Button3Click(Button3);
end;

procedure TFormMain.CBA1Click(Sender: TObject);                //C:=B*A
begin
PageControl1.Pages[2].Show;
Button4Click(Button4);
end;

procedure TFormMain.CAB1Click(Sender: TObject);                //C:=A+B
begin
PageControl1.Pages[3].Show;
Button2Click(Button2);
end;

procedure TFormMain.CA11Click(Sender: TObject);               //C:=(A)^(-1)
begin
PageControl1.Pages[4].Show;
PageControl2.Pages[0].Show;
Button6Click(Button6);
end;

procedure TFormMain.CB11Click(Sender: TObject);               //C:=(B)^(-1)
begin
PageControl1.Pages[4].Show;
PageControl2.Pages[1].Show;
Button7Click(Button7);
end;

procedure TFormMain.N29Click(Sender: TObject);            //����� �������
begin
PageControl1.Pages[5].Show;
Button13Click(Button13);
end;

procedure TFormMain.CA1B1Click(Sender: TObject);         //��������� ������
begin
PageControl1.Pages[5].Show;
Button12Click(Button12);
end;

procedure TFormMain.N32Click(Sender: TObject);             //����� �����
begin
PageControl1.Pages[5].Show;
Button14Click(Button14);
end;

procedure TFormMain.AAT1Click(Sender: TObject);               //C:=(A)T
begin
PageControl1.Pages[6].Show;
Button16Click(Button16);
end;

procedure TFormMain.BBT1Click(Sender: TObject);               //C:=(B)T
begin
PageControl1.Pages[6].Show;
Button17Click(Button17);
end;

procedure TFormMain.CAT1Click(Sender: TObject);               //C:=(A*)T
begin
PageControl1.Pages[4].Show;
Button10Click(Button10);
end;

procedure TFormMain.CBT1Click(Sender: TObject);                 //C:=(B*)T
begin
PageControl1.Pages[4].Show;
Button11Click(Button11);
end;

//Menu �������

procedure TFormMain.N42Click(Sender: TObject);               //��������
begin
//FormResult.Memo1.Lines.LoadFromFile(ExtractFilePath(ParamStr(0))+'Result.txt');
FormResult.Show;
FormResult.Button1Click(Button1);
end;

procedure TFormMain.N22Click(Sender: TObject);              //��������
begin
FormResult.Memo1.Lines.Clear;
Rewrite(ResultFile);
CloseFile(ResultFile);
end;

procedure TFormMain.N20Click(Sender: TObject);               //������
begin
FormResult.Show;
FormResult.Button1Click(Button1);
if PrinterSetupDialog1.Execute then
  if PrintDialog1.Execute
    then PrintStrings(FormResult.Memo1.Lines);
end;

procedure TFormMain.N21Click(Sender: TObject);              //Result.txt
begin
ShellExecute(0,'',pchar(ExtractFilePath(ParamStr(0))+'Result.txt'),'','',1);
end;

procedure TFormMain.N27Click(Sender: TObject);            //��������� ���
begin
if SaveDialog2.Execute then
  SaveLog(SaveDialog2.FileName);
end;

//Menu �������

procedure TFormMain.N30Click(Sender: TObject);           //������� �������
begin
Application.MessageBox(PChar('Q..E    - ����� ������� ������ ������'+#10#13
  +'A..H     - ����� ������� ������� ������'+#10#13+#10#13
  +'[ � ;     - ��������� ��������� m �������'+#10#13
  +'] � '+#39+'     - ��������� ��������� n �������'+#10#13
  +'=        - ����� �� m � n'+#10#13#10#13
  +'Tab            - ������������� ������'+#10#13
  +'Shift+Tab   - ������������� �����'+#10#13
  +'Enter          - ������������� ����'+#10#13
  +'Shift+Enter  - ������������� �����'+#10#13

  +'Ctrl+<�������> - ����������� �� �������'+#10#13#10#13
  +'F3     - �������������� ���������� �������� ������� A '+#10#13
  +'F4     - �������������� ���������� �������� ������� B '+#10#13
  +'F5     - ������������ ������� A'+#10#13
  +'F6     - ������������ ������� B'+#10#13
  +'F7     - ������������ ������ A � B'+#10#13
  +'F8     - ������������ ������ B � A'+#10#13
  +'F9     - ����� ������ A � B'+#10#13
  +'F11   - ���������� �������� ������� A'+#10#13
  +'F12   - ���������� �������� ������� B'+#10#13#10#13
  +'Ctrl+W   - �������� �����-������ Result.txt'+#10#13
  +'Ctrl+Q   - ����� �� ���������'+#10#13),
  PChar('������� �������'),MB_IconInformation+MB_OK)
end;

procedure TFormMain.N37Click(Sender: TObject);        //������� ������
begin
Application.MessageBox(PChar('������� A ������������� ��������'+#10#13+'������������ ��� x1, x2, ..., xn'+#10#13#10#13
  +'������� B ������������� ��������'+#10#13+'��������� ������'+#10#13#10#13
  +'����� ���������� ������������� ������� [C],'+#10#13+'� ������� [C](1,1)=x1; [C](2,1)=x2; ...; [C](n,1)=xn'+#10#13#10#13
  +'��������:'+#10#13
  +'4*x1 - 1*x2  = -6'+#10#13
  +'3*x1 + 2*x3 +5*x3 = -14'+#10#13
  +'x1 - 3*x2 +4*x3 = -19'+#10#13#10#13
  +'[A] ='+#10#13
  +'4|-1|0|'+#10#13
  +'3| 2|5|'+#10#13
  +'1|-3|4|'+#10#13#10#13
  +'[B] ='+#10#13
  +'  -6|'+#10#13
  +'-14|'+#10#13
  +'-19|'+#10#13#10#13
  +'[C] ='+#10#13
  +'-1|'+#10#13
  +' 2|'+#10#13
  +'-3|'+#10#13),
  PChar('������� ������ ���������'),MB_IconInformation+MB_OK);
end;

procedure TFormMain.ReadMetxt1Click(Sender: TObject);       //ReadMe.txt
begin
if FileExists(extractfilepath(paramstr(0))+'ReadMe.txt')
  then ShellExecute(0,'',pchar(extractfilepath(paramstr(0))+'ReadMe.txt'),'','',1)
  else Application.MessageBox(pchar('������ ��� ������ ����� ��� ���� �� ������'
    +#13+extractfilepath(paramstr(0))+'ReadMe.txt'),pchar(FormMain.Caption),MB_ICONERROR+mb_OK);
end;

procedure TFormMain.N6Click(Sender: TObject);               //� ���������
begin
FormAbout.ShowModal;
end;


procedure TFormMain.SpinEdit5Change(Sender: TObject);
begin
if SpinEdit5.Value=0 then SpinEdit6.Value:=1
  else SpinEdit6.Value:=0;
end;

procedure TFormMain.SpinEdit6Change(Sender: TObject);
begin
if SpinEdit6.Value=0 then SpinEdit5.Value:=1
  else SpinEdit5.Value:=0;
end;

procedure TFormMain.SpinEdit7Change(Sender: TObject);
begin
if SpinEdit7.Value=0 then SpinEdit8.Value:=1
  else SpinEdit8.Value:=0;
end;

procedure TFormMain.SpinEdit8Change(Sender: TObject);
begin
if SpinEdit8.Value=0 then SpinEdit7.Value:=1
  else SpinEdit7.Value:=0;
end;

procedure TFormMain.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
if Shift=[] then
  begin
  case Key of
    65: PageControl1.TabIndex:=0;
    83: PageControl1.TabIndex:=1;
    68: PageControl1.TabIndex:=2;
    70: PageControl1.TabIndex:=3;
    71: PageControl1.TabIndex:=4;
    72: PageControl1.TabIndex:=5;

    219:
      case PageControl2.TabIndex of
        0: if Spin_m.Enabled then Spin_m.Value:=Spin_m.Value+1;
        1: if Spin_mm.Enabled then Spin_mm.Value:=Spin_mm.Value+1;
        end;
    186:
      case PageControl2.TabIndex of
        0: if Spin_m.Enabled then Spin_m.Value:=Spin_m.Value-1;
        1: if Spin_mm.Enabled then Spin_mm.Value:=Spin_mm.Value-1;
        end;
    221:
      case PageControl2.TabIndex of
        0: Spin_n.Value:=Spin_n.Value+1;
        1: Spin_nn.Value:=Spin_nn.Value+1;
        end;
    222:
      case PageControl2.TabIndex of
        0: Spin_n.Value:=Spin_n.Value-1;
        1: Spin_nn.Value:=Spin_nn.Value-1;
        end;

    187:
      case PageControl2.TabIndex of
        0: Label4Click(Label4);
        1: Label7Click(Label7);
        end;
    81: PageControl2.TabIndex:=0;
    87: PageControl2.TabIndex:=1;
    69: PageControl2.TabIndex:=2;
    end;
  end else
  begin
  if Shift=[ssCtrl] then
    begin
  case PageControl2.TabIndex of
    0:
    case Key of
      37: SelectCellHorizontal(StringGrid1, cI1, cJ1, -1);
      38: SelectCellVertical(StringGrid1, cI1, cJ1, -1);
      39: SelectCellHorizontal(StringGrid1, cI1, cJ1, 1);
      40: SelectCellVertical(StringGrid1, cI1, cJ1, 1);
      end;
    1:
    case Key of
      37: SelectCellHorizontal(StringGrid2, cI2, cJ2, -1);
      38: SelectCellVertical(StringGrid2, cI2, cJ2, -1);
      39: SelectCellHorizontal(StringGrid2, cI2, cJ2, 1);
      40: SelectCellVertical(StringGrid2, cI2, cJ2, 1);
      end;
    2:
    case Key of
      37: SelectCellHorizontal(StringGrid3, cI3, cJ3, -1);
      38: SelectCellVertical(StringGrid3, cI3, cJ3, -1);
      39: SelectCellHorizontal(StringGrid3, cI3, cJ3, 1);
      40: SelectCellVertical(StringGrid3, cI3, cJ3, 1);
      end;
    end;
    Key:=0;
    end;
  end

end;

procedure TFormMain.PrintStrings (Strings: TStrings);
var Prn: TextFile;
    i: Integer;
begin
AssignPrn(Prn);
try
  Rewrite(Prn);
  try
    WriteLn(prn,'');
    for i := 0 to Strings.Count - 1 do
      WriteLn(Prn,'  '+ Strings.Strings[i]);
    finally
    CloseFile(Prn);
    end;
  except
  on EInOutError do
    Application.MessageBox('������. ���������� �����������',PChar(FormMain.Caption),MB_ICONERROR+mb_Ok);
  end;
end;



end.
