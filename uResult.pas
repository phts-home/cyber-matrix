unit uResult;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls;

type
  TFormResult = class(TForm)
    Memo1: TMemo;
    Button1: TButton;
    Button2: TButton;
    Button3: TButton;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure Button3Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormResult: TFormResult;

implementation

uses uMain;

{$R *.dfm}

procedure TFormResult.Button1Click(Sender: TObject);
begin
Memo1.Lines.LoadFromFile(ExtractFilePath(ParamStr(0))+'Result.txt');
Memo1.SelStart:=Length(Memo1.Text);
Memo1.Perform(EM_SCROLLCARET, 0, 0);
//Memo1.SelStart:=Memo1.Lines.Count;//.Lines[Memo1.Lines.Count];
end;

procedure TFormResult.Button2Click(Sender: TObject);
begin
Close;
end;

procedure TFormResult.FormResize(Sender: TObject);
begin
Memo1.Width:=FormResult.Width-30;
Memo1.Height:=FormResult.Height-91;
Button1.Left:=FormResult.Width-175;
Button2.Left:=FormResult.Width-95;
Button3.Left:=FormResult.Width-255;
Button1.Top:=FormResult.Height-67;
Button2.Top:=FormResult.Height-67;
Button3.Top:=FormResult.Height-67;
end;

procedure TFormResult.FormCreate(Sender: TObject);
begin
Width:=FormMain.cfg.ReadInteger('resultform','width',265);
Height:=FormMain.cfg.ReadInteger('resultform','height',337);
Left:=FormMain.Left+331;
Top:=FormMain.Top+112;
end;

procedure TFormResult.FormShow(Sender: TObject);
begin
Memo1.SetFocus;
end;

procedure TFormResult.Button3Click(Sender: TObject);
begin
FormMain.N22Click(FormMain.N22);
Memo1.SetFocus;
end;

end.
