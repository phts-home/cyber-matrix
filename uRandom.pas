unit uRandom;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Spin;

type
  TFormRandom = class(TForm)
    Label2: TLabel;
    Button2: TButton;
    Label1: TLabel;
    SpinEdit1: TSpinEdit;
    SpinEdit2: TSpinEdit;
    procedure Button2Click(Sender: TObject);
    procedure SpinEdit1KeyPress(Sender: TObject; var Key: Char);
    procedure Label2Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormRandom: TFormRandom;

implementation

uses uMain;

{$R *.dfm}

procedure TFormRandom.Button2Click(Sender: TObject);
var x, y: Byte;
begin
Randomize;
case FormMain.PageControl2.TabIndex of
  0:
    begin
    for x:=0 to FormMain.StringGrid1.ColCount-1 do
      for y:=0 to FormMain.StringGrid1.RowCount-1 do
        begin
        FormMain.StringGrid1.Cells[x,y]:=IntToStr(Random(SpinEdit2.Value-SpinEdit1.Value+1)+SpinEdit1.Value);
        end;
    FormMain.SelectCellHorizontal(FormMain.StringGrid1,0,0,0);
    end;
  1:
    begin
    for x:=0 to FormMain.StringGrid2.ColCount-1 do
      for y:=0 to FormMain.StringGrid2.RowCount-1 do
        begin
        FormMain.StringGrid2.Cells[x,y]:=IntToStr(Random(SpinEdit2.Value-SpinEdit1.Value+1)+SpinEdit1.Value);
        end;
    FormMain.SelectCellHorizontal(FormMain.StringGrid2,0,0,0);
    end;
  end;
end;

procedure TFormRandom.SpinEdit1KeyPress(Sender: TObject; var Key: Char);
begin
if not((Key>=#48)and(Key<=#57)or(Key=#45)or(Key=#8)) then Key:=#0;
end;

procedure TFormRandom.Label2Click(Sender: TObject);
begin
if Label2.Caption='[ A ]' then
  begin
  FormMain.PageControl2.Pages[1].Show;
  Label2.Caption:='[ B ]';
  end else
  begin
  FormMain.PageControl2.Pages[0].Show;
  Label2.Caption:='[ A ]';
  end;
end;

procedure TFormRandom.FormCreate(Sender: TObject);
begin
FormRandom.Left:=FormMain.Left+120;
FormRandom.Top:=FormMain.Top+40;
end;

end.
