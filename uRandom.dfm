object FormRandom: TFormRandom
  Left = 207
  Top = 144
  BorderStyle = bsToolWindow
  Caption = #1047#1072#1087#1086#1083#1085#1080#1090#1100' '#1089#1083#1091#1095#1072#1081#1085#1099#1084#1080' '#1095#1080#1089#1083#1072#1084#1080
  ClientHeight = 145
  ClientWidth = 253
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Label2: TLabel
    Left = 10
    Top = 110
    Width = 55
    Height = 22
    Caption = '[ A ]'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'Courier New'
    Font.Style = [fsBold]
    ParentFont = False
    OnClick = Label2Click
  end
  object Label1: TLabel
    Left = 75
    Top = 35
    Width = 9
    Height = 13
    Caption = '...'
  end
  object Button2: TButton
    Left = 160
    Top = 105
    Width = 75
    Height = 25
    Caption = #1042#1099#1087#1086#1083#1085#1080#1090#1100
    Default = True
    TabOrder = 0
    OnClick = Button2Click
  end
  object SpinEdit1: TSpinEdit
    Left = 20
    Top = 30
    Width = 46
    Height = 22
    MaxValue = 0
    MinValue = 0
    TabOrder = 1
    Value = 0
    OnKeyPress = SpinEdit1KeyPress
  end
  object SpinEdit2: TSpinEdit
    Left = 95
    Top = 30
    Width = 46
    Height = 22
    MaxValue = 0
    MinValue = 0
    TabOrder = 2
    Value = 10
    OnKeyPress = SpinEdit1KeyPress
  end
end
