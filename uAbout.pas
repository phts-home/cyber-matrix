unit uAbout;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, StdCtrls, ShellAPI;

type
  TFormAbout = class(TForm)
    Image1: TImage;
    Button1: TButton;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label1: TLabel;
    procedure Label9Click(Sender: TObject);
    procedure Label10Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormAbout: TFormAbout;

implementation

uses uMain;

{$R *.dfm}

procedure TFormAbout.Label9Click(Sender: TObject);
begin
ShellExecute(0,'',pchar('mailto:'+Label9.Caption+'?subject=Cyber Matrix'),'','',1);
end;

procedure TFormAbout.Label10Click(Sender: TObject);
begin
ShellExecute(0,'',pchar(Label10.Caption),'','',1);
end;

procedure TFormAbout.FormShow(Sender: TObject);
begin
FormAbout.Left:=FormMain.Left+81;
FormAbout.Top:=FormMain.Top+105;
end;

end.
