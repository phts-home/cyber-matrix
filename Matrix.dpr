program Matrix;

uses
  Forms,
  uMain in 'uMain.pas' {FormMain},
  uAbout in 'uAbout.pas' {FormAbout},
  uAdd in 'uAdd.pas' {FormAdd},
  uRandom in 'uRandom.pas' {FormRandom},
  uResult in 'uResult.pas' {FormResult};

{$R *.res}

begin
  Application.Initialize;
  Application.Title := 'Cyber Matrix';
  Application.CreateForm(TFormMain, FormMain);
  Application.CreateForm(TFormAbout, FormAbout);
  Application.CreateForm(TFormAdd, FormAdd);
  Application.CreateForm(TFormRandom, FormRandom);
  Application.CreateForm(TFormResult, FormResult);
  Application.Run;
end.
