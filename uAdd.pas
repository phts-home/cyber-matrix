unit uAdd;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Buttons, Spin, StdCtrls;

type
  TFormAdd = class(TForm)
    Edit1: TEdit;
    Label1: TLabel;
    SpinEdit5: TSpinEdit;
    Label11: TLabel;
    Label13: TLabel;
    SpinEdit7: TSpinEdit;
    SpinEdit8: TSpinEdit;
    Label14: TLabel;
    Label12: TLabel;
    SpinEdit6: TSpinEdit;
    SpeedButton1: TSpeedButton;
    SpeedButton2: TSpeedButton;
    SpeedButton3: TSpeedButton;
    SpeedButton4: TSpeedButton;
    Label2: TLabel;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure Edit1KeyPress(Sender: TObject; var Key: Char);
    procedure Label2Click(Sender: TObject);
    procedure Edit1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormAdd: TFormAdd;

implementation

uses uMain;

{$R *.dfm}

procedure TFormAdd.SpeedButton1Click(Sender: TObject);
var Temp: String;
    x: Integer;
begin
case FormMain.PageControl2.TabIndex of
  0:
    begin
    for x:=0 to FormMain.StringGrid1.ColCount-1 do
      begin
      Temp:=FormMain.StringGrid1.Cells[x,SpinEdit5.Value-1];
      FormMain.StringGrid1.Cells[x,SpinEdit5.Value-1]:=FormMain.StringGrid1.Cells[x,SpinEdit7.Value-1];
      FormMain.StringGrid1.Cells[x,SpinEdit7.Value-1]:=Temp;
      end;
    FormMain.SelectCellHorizontal(FormMain.StringGrid1,0,0,0);
    end;
  1:
    begin
    for x:=0 to FormMain.StringGrid2.ColCount-1 do
      begin
      Temp:=FormMain.StringGrid2.Cells[x,SpinEdit5.Value-1];
      FormMain.StringGrid2.Cells[x,SpinEdit5.Value-1]:=FormMain.StringGrid2.Cells[x,SpinEdit7.Value-1];
      FormMain.StringGrid2.Cells[x,SpinEdit7.Value-1]:=Temp;
      end;
    FormMain.SelectCellHorizontal(FormMain.StringGrid2,0,0,0);
    end;
  end;
end;

procedure TFormAdd.SpeedButton2Click(Sender: TObject);
var Temp: String;
    x: Integer;
begin
case FormMain.PageControl2.TabIndex of
  0:
    begin
    for x:=0 to FormMain.StringGrid1.RowCount-1 do
      begin
      Temp:=FormMain.StringGrid1.Cells[SpinEdit6.Value-1,x];
      FormMain.StringGrid1.Cells[SpinEdit6.Value-1,x]:=FormMain.StringGrid1.Cells[SpinEdit8.Value-1,x];
      FormMain.StringGrid1.Cells[SpinEdit8.Value-1,x]:=Temp;
      end;
    FormMain.SelectCellHorizontal(FormMain.StringGrid1,0,0,0);
    end;
  1:
    begin
     for x:=0 to FormMain.StringGrid2.RowCount-1 do
      begin
      Temp:=FormMain.StringGrid2.Cells[SpinEdit6.Value-1,x];
      FormMain.StringGrid2.Cells[SpinEdit6.Value-1,x]:=FormMain.StringGrid2.Cells[SpinEdit8.Value-1,x];
      FormMain.StringGrid2.Cells[SpinEdit8.Value-1,x]:=Temp;
      end;
    FormMain.SelectCellHorizontal(FormMain.StringGrid2,0,0,0);
    end;
  end;
end;

procedure TFormAdd.SpeedButton3Click(Sender: TObject);
var x: Integer;
begin
case FormMain.PageControl2.TabIndex of
  0:
    begin
    for x:=0 to FormMain.StringGrid1.ColCount-1 do
      begin
      FormMain.StringGrid1.Cells[x,SpinEdit7.Value-1]:=FloatToStr(StrToFloat(FormMain.StringGrid1.Cells[x,SpinEdit5.Value-1])*StrToFloat(Edit1.Text)
          +StrToFloat(FormMain.StringGrid1.Cells[x,SpinEdit7.Value-1]));
      end;
    FormMain.SelectCellHorizontal(FormMain.StringGrid1,0,0,0);
    end;
  1:
    begin
    for x:=0 to FormMain.StringGrid2.ColCount-1 do
      begin
      FormMain.StringGrid2.Cells[x,SpinEdit7.Value-1]:=FloatToStr(StrToFloat(FormMain.StringGrid2.Cells[x,SpinEdit5.Value-1])*StrToFloat(Edit1.Text)
          +StrToFloat(FormMain.StringGrid2.Cells[x,SpinEdit7.Value-1]));
      end;
    FormMain.SelectCellHorizontal(FormMain.StringGrid2,0,0,0);
    end;
  end;
end;

procedure TFormAdd.SpeedButton4Click(Sender: TObject);
var x: Integer;
begin
case FormMain.PageControl2.TabIndex of
  0:
    begin
    for x:=0 to FormMain.StringGrid1.RowCount-1 do
      begin
      FormMain.StringGrid1.Cells[SpinEdit8.Value-1,x]:=FloatToStr(StrToFloat(FormMain.StringGrid1.Cells[SpinEdit6.Value-1,x])*StrToFloat(Edit1.Text)
          +StrToFloat(FormMain.StringGrid1.Cells[SpinEdit8.Value-1,x]));
      end;
    FormMain.SelectCellHorizontal(FormMain.StringGrid1,0,0,0);
    end;
  1:
    begin
     for x:=0 to FormMain.StringGrid2.RowCount-1 do
      begin
      FormMain.StringGrid2.Cells[SpinEdit8.Value-1,x]:=FloatToStr(StrToFloat(FormMain.StringGrid2.Cells[SpinEdit6.Value-1,x])*StrToFloat(Edit1.Text)
          +StrToFloat(FormMain.StringGrid2.Cells[SpinEdit8.Value-1,x]));
      end;
    FormMain.SelectCellHorizontal(FormMain.StringGrid2,0,0,0);
    end;
  end;
end;

procedure TFormAdd.Edit1KeyPress(Sender: TObject; var Key: Char);
begin
if not((Key>=#48)and(Key<=#57)or(Key=#44)or(Key=#45)or(Key=#8)) then Key:=#0;
end;

procedure TFormAdd.Label2Click(Sender: TObject);
begin
if Label2.Caption='[ A ]' then
  begin
  FormMain.PageControl2.Pages[1].Show;
  Label2.Caption:='[ B ]';
  end else
  begin
  FormMain.PageControl2.Pages[0].Show;
  Label2.Caption:='[ A ]';
  end;
end;

procedure TFormAdd.Edit1Click(Sender: TObject);
begin
Edit1.SelectAll;
end;

procedure TFormAdd.FormCreate(Sender: TObject);
begin
FormAdd.Left:=FormMain.Left+120;
FormAdd.Top:=FormMain.Top+40;
end;

end.
